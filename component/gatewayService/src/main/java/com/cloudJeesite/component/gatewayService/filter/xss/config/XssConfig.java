package com.cloudJeesite.component.gatewayService.filter.xss.config;


import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cloudJeesite.component.gatewayService.filter.xss.XssFilter;
import com.google.common.collect.Maps;

@Configuration
public class XssConfig {
	// 包含key
	@Value("${benefitech.containKey}")
	private String containKey;
	// 过滤前缀依据
	@Value("${benefitech.filterPrefix}")
	private String filterPrefix;

	/**
	 * xss过滤拦截器
	 */
	@Bean
	public FilterRegistrationBean xssFilterRegistrationBean() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setFilter(new XssFilter(containKey,filterPrefix));
		filterRegistrationBean.setOrder(0);
		filterRegistrationBean.setEnabled(true);
		filterRegistrationBean.addUrlPatterns("/*");
		Map<String, String> initParameters = Maps.newHashMap();
		initParameters.put("excludes", "*.css,*.js,*/api/,*/img/,*/fonts/,*.map,*.woff2,*.ico,*.png,*.jpg,*.gif");
//		initParameters.put("excludes", "*.css,*.js,*/api/,*/img/,*/fonts/,*.map,*.woff2,*.ico,*.png,*.jpg,*.gif,*/bc/portal/");
		initParameters.put("isIncludeRichText", "true");
		filterRegistrationBean.setInitParameters(initParameters);
		return filterRegistrationBean;
	}
}