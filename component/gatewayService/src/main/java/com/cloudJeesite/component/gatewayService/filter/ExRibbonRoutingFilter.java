package com.cloudJeesite.component.gatewayService.filter;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.ribbon.support.RibbonRequestCustomizer;
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper;
import org.springframework.cloud.netflix.zuul.filters.route.RibbonCommandFactory;
import org.springframework.cloud.netflix.zuul.filters.route.RibbonRoutingFilter;
import org.springframework.http.client.ClientHttpResponse;

import com.netflix.hystrix.exception.HystrixRuntimeException;
import com.netflix.zuul.exception.ZuulException;

/**
 * Extend {@link RibbonRoutingFilter} by log additional request info when handling Hystrix exception
 */
public class ExRibbonRoutingFilter extends RibbonRoutingFilter {

    private static final Logger LOG = LoggerFactory.getLogger(ExRibbonRoutingFilter.class);

    @SuppressWarnings("rawtypes")
	public ExRibbonRoutingFilter(ProxyRequestHelper helper, RibbonCommandFactory<?> ribbonCommandFactory, List<RibbonRequestCustomizer> requestCustomizers) {
        super(helper, ribbonCommandFactory, requestCustomizers);
    }

    @Override
    protected ClientHttpResponse handleException(Map<String, Object> info,
                                                 HystrixRuntimeException ex) throws ZuulException {
        this.logInfo(info);  // get additional info logged (typically it's collected by TraceProxyRequestHelper

        return super.handleException(info, ex);
    }

    private void logInfo(Map<String, Object> info) {
        LOG.warn("Exception occur while routing. Additional info is: {}", info);
    }

}
