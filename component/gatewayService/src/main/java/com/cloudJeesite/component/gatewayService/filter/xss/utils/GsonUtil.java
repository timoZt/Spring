package com.cloudJeesite.component.gatewayService.filter.xss.utils;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUtil {
	/**
	 * JSON对象转换成PO
	 * @param json json字符串
	 * @param clazz 转换的类型 
	 * @return Object
	 */
	public static Object decrypt(String json, Class<?> clazz) {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();  
		return gson.fromJson(json, clazz);

	}
	/**
	 * 对象转换成JSON对象
	 * @param obj PO对象
	 * @return String
	 */
	public static String encrypt(Object obj) {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();  
		return gson.toJson(obj);
		
	}
}
