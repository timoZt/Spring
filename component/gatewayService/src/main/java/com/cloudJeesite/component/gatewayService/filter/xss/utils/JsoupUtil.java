package com.cloudJeesite.component.gatewayService.filter.xss.utils;


import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;  
  
/** 
 * xss非法标签过滤工具类 
 * 过滤html中的xss字符 
 * @author Star.Guo 
 */  
public class JsoupUtil {  
  
    /** 
     * 使用自带的basicWithImages 白名单 
     * 允许的便签有a,b,blockquote,br,cite,code,dd,dl,dt,em,i,li,ol,p,pre,q,small,span, 
     * strike,strong,sub,sup,u,ul,img 
     * 以及a标签的href,img标签的src,align,alt,height,width,title属性 
     */  
    private static final Whitelist whitelist = Whitelist.basicWithImages();  
    /** 配置过滤化参数,不对代码进行格式化 */  
    private static final Document.OutputSettings outputSettings = new Document.OutputSettings().prettyPrint(false);  
    static {  
        // 富文本编辑时一些样式是使用style来进行实现的  
        // 比如红色字体 style="color:red;"  
        // 所以需要给所有标签添加style属性  
        whitelist.addAttributes(":all", "style");  
    }  
  
    public static String clean(String content) {  
        if(StringUtils.isEmpty(content)){  
           return content;
        }  
        return Jsoup.clean(content, "", whitelist, outputSettings);  
    }  
	@SuppressWarnings("unused")
	private static String cleanXSS(String value) {
		value = value.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
		value = value.replaceAll("%3C", "&lt;").replaceAll("%3E", "&gt;");
		value = value.replaceAll("\\(", "&#40;").replaceAll("\\)", "&#41;");
		value = value.replaceAll("%28", "&#40;").replaceAll("%29", "&#41;");
		value = value.replaceAll("'", "&#39;");
		value = value.replaceAll("eval\\((.*)\\)", "");
		value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		value = value.replaceAll("script", "");
		return value;
	}
}  