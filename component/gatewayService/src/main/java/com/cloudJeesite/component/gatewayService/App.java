package com.cloudJeesite.component.gatewayService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
/**
 * 路由网关
 * http://localhost:8769/api-a/hi?name=forezp
 * 作为Zipkin客户端，需要将链路数据上传给Zipkin Server，同时它也作为Eureka Client
 * 需要先启动Zipkin
 */
@EnableZuulProxy
@EnableEurekaClient
@SpringBootApplication
public class App {
	
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}