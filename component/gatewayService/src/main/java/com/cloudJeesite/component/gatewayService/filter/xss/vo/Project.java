package com.cloudJeesite.component.gatewayService.filter.xss.vo;


import java.io.Serializable;

/**
 * 项目权限对象
 * @author Star.Guo
 *
 */
public class Project implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7383270761089862883L;
	// 项目Id
	private String proId;

	public String getProId() {
		return proId;
	}

	public void setProId(String proId) {
		this.proId = proId;
	}

}
