package com.cloudJeesite.component.configServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 配置中心服务器
 * @author Administrator
 *
 */
@SpringBootApplication
@EnableConfigServer
@EnableEurekaClient
public class App{

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}