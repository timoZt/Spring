package com.cloudJeesite.component.eurekaServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/** 
* @author timo E-mail: 13551883869@sina.cn
* @version 创建时间：2018年4月4日 下午1:58:22 
* Eureka注册中心 
*/
@SpringBootApplication
@EnableEurekaServer
public class App {
	
	  public static void main(String[] args) {
	        SpringApplication.run(App.class,args);
	  }
	  
}
