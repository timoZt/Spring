package com.cloudJeesite.component.zipkinServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import zipkin.server.EnableZipkinServer;

/**
 * 服务链路追踪(Spring Cloud Sleuth) zipkin
 * @author timo E-mail: 
 * @version 创建时间：2018年5月3日 下午2:46:15 
 * 访问地址为ip+端口
 * 安装教程
 * http://blog.didispace.com/spring-boot-rabbitmq/
 */
@SpringBootApplication
@EnableEurekaClient
@EnableZipkinServer
public class App {
	
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
