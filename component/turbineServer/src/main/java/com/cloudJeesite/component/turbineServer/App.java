package com.cloudJeesite.component.turbineServer;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

/**
 * 断路器聚合监控(Hystrix Turbine)
 * @author timo
 */
@SpringBootApplication
@EnableTurbine
//注解@EnableTurbine，开启turbine
//@EnableTurbine注解包含了@EnableDiscoveryClient注解，即开启了注册服务。
public class App {

    public static void main(String[] args) {
        new SpringApplicationBuilder(App.class).web(true).run(args);
    }
    
//    http://localhost:8769/turbine.stream,
//    打开:http://localhost:8763/hystrix,输入监控流http://localhost:8769/turbine.stream
}