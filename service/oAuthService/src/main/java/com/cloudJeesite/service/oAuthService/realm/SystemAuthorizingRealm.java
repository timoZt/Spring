///**
// * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
// */
//package com.cloudJeesite.service.oAuthService.realm;
//
//import java.io.Serializable;
//import java.util.Collection;
//import java.util.List;
//
//import org.apache.commons.lang3.StringUtils;
//import org.apache.shiro.authc.AuthenticationException;
//import org.apache.shiro.authc.AuthenticationInfo;
//import org.apache.shiro.authc.AuthenticationToken;
//import org.apache.shiro.authc.SimpleAuthenticationInfo;
//import org.apache.shiro.authz.AuthorizationInfo;
//import org.apache.shiro.authz.Permission;
//import org.apache.shiro.authz.SimpleAuthorizationInfo;
//import org.apache.shiro.cache.Cache;
//import org.apache.shiro.realm.AuthorizingRealm;
//import org.apache.shiro.session.Session;
//import org.apache.shiro.subject.PrincipalCollection;
//import org.apache.shiro.subject.SimplePrincipalCollection;
//import org.apache.shiro.util.ByteSource;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import com.springBoot.springBootSysCore.common.config.Global;
//import com.springBoot.springBootSysCore.common.utils.CacheUtils;
//import com.springBoot.springBootSysCore.common.utils.SpringContextHolder;
//import com.springBoot.springBootSysCore.common.utils.encoding.Encodes;
//import com.springBoot.springBootSysCore.common.web.Servlets;
//import com.springBoot.springBootSysCore.modules.entity.system.SysResource;
//import com.springBoot.springBootSysCore.modules.entity.system.SysRole;
//import com.springBoot.springBootSysCore.modules.entity.system.SysUser;
//import com.springBoot.springBootSysCore.modules.entity.utils.LogUtils;
//import com.springBoot.springBootSysCore.modules.entity.utils.UserUtils;
//import com.springBoot.springBootSysCore.modules.enums.base.UserTypeEnum;
//import com.springBoot.springBootSysCore.modules.services.SystemService;
//import com.springBoot.springBootSysCore.modules.shiro.error.KickoutException;
//
///**
// * 系统安全认证实现类
// * @author ThinkGem
// * @version 2014-7-5
// */
//@Component
////@DependsOn({"userDao","roleDao","menuDao"})
//public class SystemAuthorizingRealm extends AuthorizingRealm {
//
//	private Logger logger = LoggerFactory.getLogger(getClass());
//	@Autowired
//	private SystemService systemService;
//
//	public SystemAuthorizingRealm() {
//		this.setCachingEnabled(false);
//	}
//
//	/**
//	 * 认证回调函数, 登录时调用
//	 */
//	@Override
//	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) {
//		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
//		if(token.isMobileLogin()){
//			//校验验证码--手机
//			Object e= CacheUtils.get("msgCodeCache",token.getUsername()+"msgCode");
//			if(e==null||e==""){
//				throw new AuthenticationException("msg:验证码已过期,请重新获取验证码.");
//			}
//			if(!token.getCaptcha().equals(e)){
//				throw new AuthenticationException("msg:验证码错误, 请重试.");
//			}
//		}
//		int activeSessionSize = getSystemService().getSessionDao().getActiveSessions(false).size();
//		if (logger.isDebugEnabled()){
//			logger.debug("login submit, active session size: {}, username: {}", activeSessionSize, token.getUsername());
//		}
//		if(StringUtils.isBlank(token.getUsername())){
//			throw new AuthenticationException("msg:用户名不能为空.");
//		}
//		if(token.getPassword().length==0){
//			throw new AuthenticationException("msg:密码不能为空.");
//		}
//
//		// 校验登录--后台--在加密中判断
////		if (LoginController.isValidateCodeLogin(token.getUsername(), false, false)){
////			Session session = UserUtils.getSession();
////			String code = (String)session.getAttribute(ValidateCodeServlet.VALIDATE_CODE);
////			if (token.getCaptcha() == null || !token.getCaptcha().toUpperCase().equals(code)){
////				throw new AuthenticationException("msg:密码错误次数过多.");
////			}
////		}
//
//		// 校验用户名密码
//		SysUser user = getSystemService().getUserByLoginName(token.getUsername());
//		if (user != null) {
//			if (Global.TRUE.equals(user.getAvailable())){
//				throw new AuthenticationException("msg:该已帐号禁止登录.");
//			}
//			if (Global.FALSE.equals(user.getActivation())){
//				throw new AuthenticationException("msg:该已帐号尚未激活.");
//			}
//			byte[] salt = Encodes.decodeHex(user.getPassword().substring(0,16));
//			return new SimpleAuthenticationInfo(new Principal(user, token.isMobileLogin()), 
//					user.getPassword().substring(16), ByteSource.Util.bytes(salt), getName());
//		} else {
//			return null;
//		}
//	}
//	/**
//	 * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用
//	 */
//	@Override
//	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
//		Principal principal = (Principal) getAvailablePrincipal(principals);
//		// 获取当前已登录的用户
//		if (!Global.TRUE.equals(Global.getConfig("user.multiAccountLogin"))){
//			Collection<Session> sessions = getSystemService().getSessionDao().getActiveSessions(true, principal, UserUtils.getSession());
//			if (sessions.size() > 0){
//				// 如果是登录进来的，则踢出已在线用户
//				if (UserUtils.getSubject().isAuthenticated()){
//					for (Session session : sessions){
//						getSystemService().getSessionDao().delete(session);
//					}
//				}else{// 记住我进来的，并且当前用户已登录，则退出当前用户提示信息。
//					UserUtils.getSubject().logout();
//					throw new KickoutException();
//				}
//			}
//		}
//		SimpleAuthorizationInfo info = null;
//		SysUser user = getSystemService().getUserByLoginName(principal.getAccount());
//		if (user != null && UserTypeEnum.ADMIN.getVal().equals(user.getUserType())) {
////			info = (SimpleAuthorizationInfo)UserUtils.getCache(UserUtils.CACHE_AUTH_INFO);
//			if (info == null) {
//				info =  new SimpleAuthorizationInfo();
//				List<SysResource> list = UserUtils.getMenuList();
//				for (SysResource menu : list){
//					if (StringUtils.isNotBlank(menu.getCode())){
//						// 添加基于Permission的权限信息
//						info.addStringPermission(menu.getCode());
//					}
//				}
//				// 添加用户权限
////				info.addStringPermission("user");
//				// 添加用户角色信息
//				for (SysRole role : user.getRoleList()){
//					info.addRole(role.getCode());
//				}
//				if(user.getDefalutRole()!=null){
//					info.addRole(user.getDefalutRole().getName());
//					for(SysResource sysResource :user.getDefalutRole().getResources()){
//						info.addStringPermission(sysResource.getCode());
//					}
//				}
//				if(user.getAccount().equals("admin")){
//					info.addStringPermission("superAmin");
//				}
////				if (info != null) {
////					UserUtils.putCache(UserUtils.CACHE_AUTH_INFO, info);
////				}
//			}
//			System.out.println("======用户权限=========["+info.getStringPermissions()+"]==========用户权限=========");
//			//更新登录IP和时间
//			getSystemService().updateUserLoginInfo(user);
//			// 记录登录日志
//			LogUtils.saveLog(Servlets.getRequest(), "系统登录");
//			return info;
//		} else {
//			return null;
//		}
//	}
//	
//	@Override
//	protected void checkPermission(Permission permission, AuthorizationInfo info) {
//		authorizationValidate(permission);
//		super.checkPermission(permission, info);
//	}
//	
//	@Override
//	protected boolean[] isPermitted(List<Permission> permissions, AuthorizationInfo info) {
//		if (permissions != null && !permissions.isEmpty()) {
//            for (Permission permission : permissions) {
//        		authorizationValidate(permission);
//            }
//        }
//		return super.isPermitted(permissions, info);
//	}
//	
//	@Override
//	public boolean isPermitted(PrincipalCollection principals, Permission permission) {
//		authorizationValidate(permission);
//		return super.isPermitted(principals, permission);
//	}
//	
//	@Override
//	protected boolean isPermittedAll(Collection<Permission> permissions, AuthorizationInfo info) {
//		if (permissions != null && !permissions.isEmpty()) {
//            for (Permission permission : permissions) {
//            	authorizationValidate(permission);
//            }
//        }
//		return super.isPermittedAll(permissions, info);
//	}
//	/**
//	 * 授权验证方法
//	 * @param permission
//	 */
//	private void authorizationValidate(Permission permission){
//		// 模块授权预留接口
//	}
//	
//	/**
//	 * 设定密码校验的Hash算法与迭代次数 在config中配置
//	 * 被@PostConstruct修饰的方法会在服务器加载Servlet的时候运行，并且只会被服务器调用一次，类似于Serclet的inti()方法。
//	 * 被@PostConstruct修饰的方法会在构造函数之后，init()方法之前运行
//	 *
//	 * 被@PreConstruct修饰的方法会在服务器卸载Servlet的时候运行，并且只会被服务器调用一次，类似于Servlet的destroy()方法。
//	 * 被@PreConstruct修饰的方法会在destroy()方法之后运行，在Servlet被彻底卸载之前。（详见下面的程序实践）
//	 */
////	@PostConstruct
////	public void initCredentialsMatcher() {
////		HashedCredentialsMatcher matcher = new HashedCredentialsMatcher(SystemService.HASH_ALGORITHM);
////		matcher.setHashIterations(SystemService.HASH_INTERATIONS);
////		setCredentialsMatcher(matcher);
////	}
//	
//	/**
//	 * 清空用户关联权限认证，待下次使用时重新加载
//	 */
//	public void clearCachedAuthorizationInfo(Principal principal) {
//		SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
//		clearCachedAuthorizationInfo(principals);
//	}
//
//	/**
//	 * 清空所有关联认证
//	 * @Deprecated 不需要清空，授权缓存保存到session中
//	 */
//	@Deprecated
//	public void clearAllCachedAuthorizationInfo() {
//		Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
//		if (cache != null) {
//			for (Object key : cache.keys()) {
//				cache.remove(key);
//			}
//		}
//		getAuthorizationCache().clear();
//	}
////	public void clearAllCachedAuthorizationInfo() {
////		getAuthorizationCache().clear();
////	}
//	public void clearAllCachedAuthenticationInfo() {
//		getAuthenticationCache().clear();
//	}
//
//	public void clearAllCache() {
//		clearAllCachedAuthenticationInfo();
//		clearAllCachedAuthorizationInfo();
//	}
//
//
//	/*CACHE*/
////	@Override
////	public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
////		super.clearCachedAuthorizationInfo(principals);
////	}
////
////	@Override
////	public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
////		super.clearCachedAuthenticationInfo(principals);
////	}
////
////	@Override
////	public void clearCache(PrincipalCollection principals) {
////		super.clearCache(principals);
////	}
////
////	public void clearAllCachedAuthorizationInfo() {
////		getAuthorizationCache().clear();
////	}
////
////	public void clearAllCachedAuthenticationInfo() {
////		getAuthenticationCache().clear();
////	}
////
////	public void clearAllCache() {
////		clearAllCachedAuthenticationInfo();
////		clearAllCachedAuthorizationInfo();
////	}
//
//
//	/**
//	 * 获取系统业务对象
//	 */
//	public SystemService getSystemService() {
//		if (systemService == null){
//			systemService = SpringContextHolder.getBean(SystemService.class);
//		}
//		return systemService;
//	}
//	
//	/**
//	 * 授权用户信息
//	 */
//	public static class Principal implements Serializable {
//
//		private static final long serialVersionUID = 1L;
//		
//		private String id; // 编号
//		private String account; // 登录名
//		private String nickName; // 昵称
//		private boolean mobileLogin; // 是否手机登录
//		
////		private Map<String, Object> cacheMap;
//
//		public Principal(SysUser user, boolean mobileLogin) {
//			this.id = user.getId();
//			this.account = user.getAccount();
//			this.nickName = user.getNickName();
//			this.mobileLogin = mobileLogin;
//		}
//
//		public String getId() {
//			return id;
//		}
//
//		public String getAccount() {
//			return account;
//		}
//
//		public String getNickName() {
//			return nickName;
//		}
//
//		public boolean isMobileLogin() {
//			return mobileLogin;
//		}
//
////		@JsonIgnore
////		public Map<String, Object> getCacheMap() {
////			if (cacheMap==null){
////				cacheMap = new HashMap<String, Object>();
////			}
////			return cacheMap;
////		}
//
//		/**
//		 * 获取SESSIONID
//		 */
//		public String getSessionid() {
//			try{
//				return (String) UserUtils.getSession().getId();
//			}catch (Exception e) {
//				return "";
//			}
//		}
//		
//		@Override
//		public String toString() {
//			return id;
//		}
//
//	}
//}
