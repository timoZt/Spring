<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ attribute name="url" type="java.lang.String" required="true" description="数据地址"%>
<%@ attribute name="id" type="java.lang.String" required="true" description="id"%>
<%@ attribute name="name" type="java.lang.String" required="true" description="name"%>

<%@ attribute name="cssClass" type="java.lang.String" required="false" description="css样式"%>
<%@ attribute name="cssStyle" type="java.lang.String" required="false" description="css样式"%>

<%@ attribute name="value" type="java.lang.String" description="数据value"%>
<%@ attribute name="text" type="java.lang.String" description="数据text"%>
<%@ attribute name="selectValue" type="java.lang.String" description="选中数据value"%>

<select class="${cssClass}" style="${cssStyle}" id="${id}" name="${name}">
    <option value="" >请选择</option>
</select>

<script type="application/javascript">
    $.ajax({
        type: 'POST',
        dataType : "json",
        url: "${url}",
        success:function(data){
            for(var i = 0;i<data.length;i++){
                if(data[i].${value} == "${selectValue}"){
                    $("#${id}").append("<option value='"+data[i].${value}+"' selected='selected'>"+data[i].${text}+"</option>");
                }else{
                    $("#${id}").append("<option value='"+data[i].${value}+"'>"+data[i].${text}+"</option>");
                }
                <%--$("#${id}").options.add(new option(data[i].${text},data[i].${value}));--%>
            }
        }
    });
</script>