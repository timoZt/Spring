<%@ page contentType="text/html;charset=UTF-8" %>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/self/css/style.css" />
<link rel="stylesheet" href="${ctxStatic}/kitadmin/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctxStatic}/thirdparty/jquery/1.9.1/jquery.js"></script>

<style>
    /*输入框*/
    #indexSerchForm .layui-input {
        height: 30px;
        width: 120px;
    }
    /*页面引导*/
    .x-nav {
        padding: 0 20px;
        position: relative;
        z-index: 99;
        border-bottom: 1px solid #e5e5e5;
        height: 32px;
        overflow: hidden;
    }
    /*必填星号*/
    .x-red{
        color: red;
    }
    /*验证*/
    label.error {
        position: absolute;
        right: 18px;
        top: 15px;
        color: #ef392b;
        font-size: 12px;
    }
    .Validform_error, input.error, select.error {
        background-color: #fbe2e2;
        border-color: #c66161;
        color: #c00;
    }
</style>