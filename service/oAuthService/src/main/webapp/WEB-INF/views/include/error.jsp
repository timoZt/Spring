<%@ page contentType="text/html;charset=UTF-8" %>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=8,IE=9,IE=10" />
<meta http-equiv="Expires" content="0">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">

<link href="${ctxStatic}/self/jeesite.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/self/jeesite.js" type="text/javascript"></script>
<script src="${ctxStatic}/thirdparty/jquery/1.9.1/jquery.js" type="text/javascript"></script>
<link href="${ctxStatic}/thirdparty/bootstrap/2.3.1/css_${not empty cookie.theme.value ? cookie.theme.value : 'cerulean'}/bootstrap.min.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/thirdparty/bootstrap/2.3.1/js/bootstrap.min.js" type="text/javascript"></script>
