<%@ page contentType="text/html;charset=UTF-8" %>
<script type="text/javascript" src="${ctxStatic}/self/jeesite.js"></script>
<script type="text/javascript" src="${ctxStatic}/self/js/common.js"></script>

<link rel="stylesheet" type="text/css" href="${ctxStatic}/thirdparty/select2/select2.css" />
<script type="text/javascript" src="${ctxStatic}/thirdparty/select2/select2.full.js"></script>
<script type="text/javascript" src="${ctxStatic}/thirdparty/select2/i18n/zh-CN.js"></script>

<script type="text/javascript" src="${ctxStatic}/thirdparty/dateText/WdatePicker.js"></script>

<script type="text/javascript" src="${ctxStatic}/thirdparty/validform/jquery-form.js"></script>
<script type="text/javascript" src="${ctxStatic}/thirdparty/validform/jquery.validate.js" ></script>
<script type="text/javascript" src="${ctxStatic}/self/js/validata.js"></script>
<script type="text/javascript" src="${ctxStatic}/thirdparty/validform/validate-methods.js"></script>
<script type="text/javascript" src="${ctxStatic}/thirdparty/validform/messages_zh.js"></script>




<script type="text/javascript" src="${ctxStatic}/thirdparty/jquery-jbox/browser.js"></script>
<link href="${ctxStatic}/thirdparty/jquery-jbox/2.3/Skins/Bootstrap/jbox.min.css" rel="stylesheet" />
<script type="text/javascript" src="${ctxStatic}/thirdparty/jquery-jbox/2.3/jquery.jBox-2.3.min.js"></script>
<%--ztree--%>
<link href="${ctxStatic}/thirdparty/zTree/3.5.12/css/zTreeStyle/zTreeStyle.min.css" rel="stylesheet" type="text/css"/>
<script src="${ctxStatic}/thirdparty/zTree/3.5.12/js/jquery.ztree.all-3.5.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/thirdparty/zTree/3.5.12/js/jquery.ztree.exhide-3.5.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="${ctxStatic}/self/tree/alertZtreeCss.css" >
<script type="text/javascript" src="${ctxStatic }/self/tree/zTree.js"></script>
<%--webuploader--%>
<link rel="stylesheet" type="text/css" href="${ctxStatic }/thirdparty/diyUpload/css/webuploader.css">
<link rel="stylesheet" type="text/css" href="${ctxStatic }/thirdparty/diyUpload/css/diyUpload.css">
<link rel="stylesheet" type="text/css" href="${ctxStatic }/thirdparty/diyUpload/css/demo.css">
<script type="text/javascript" src="${ctxStatic }/thirdparty/diyUpload/js/webuploader.html5only.js"></script>

<script type="text/javascript" src="${ctxStatic }/thirdparty/diyUpload/js/diySimpleUpload.js"></script>
<%--通用js--%>
<script type="text/javascript" src="${ctxStatic }/thirdparty/underscore.js"></script>
<%--多标签输入input--%>
<link href="${ctxStatic}/thirdparty/tagInput/css/styles.css" rel="stylesheet" type="text/css" />
<script src="${ctxStatic}/thirdparty/tagInput/js/jquery.ui.widget.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/thirdparty/tagInput/js/jquery.manifest.js" type="text/javascript"></script>
<%--kitadmin layui--%>
<script type="text/javascript" src="${ctxStatic}/kitadmin/plugins/layui/layui.all.js" charset="utf-8"></script>

<script type="text/javascript">
var res = "${res}";
var ctx = "${ctx}";
/*oss上传*/
var oss_upfile_defaltdir = "${fns:getConfig('oss.upfile_defaltdir')}";
var oss_callBack = "<%=basePath%>${fns:getConfig('oss.cellBack')}";
var oss_postObjectPolicy = res+"${fns:getConfig('oss.postObjectPolicy')}";
var oss_url = "${fns:getConfig('oss.url')}";

(function($) {
    $.fn.disable = function () {
        return $(this).find("input").each(function () {
            $(this).attr("disabled", "disabled");
        });
    }
})(jQuery);

document.onkeydown = function (e) { // 回车提交表单 搜索
    var theEvent = window.event || e;
    var code = theEvent.keyCode || theEvent.which;
    if (code == 13) {
        $(".select .select-on").click();
    }
}
//$.validator.setDefaults({
//    submitHandler: function(form) {
////        alert("提交事件!");form.submit();
//        return  false;
//    }
//});


function layerDelCheck(url,id, flag) {
    layer.confirm('确定删除?', function (index) {
        layerDel(url,id, flag);
        location.reload();
    });
}

function layerDel(url,id, flag) {
    $.ajax({
        url:url,
        type:"post",
        data:{id:id,sureDel:flag},
        async:false,
        success:function(data){
            window.top.layer.msg(data.msg,{icon:6,offset: 'rb',area:['120px','80px'],anim:2});
        },
        error:function(){
            alert('error');
        }
    });
}
function layerDels(url,ids, flag) {
    $.ajax({
        url:url,
        type:"post",
        data:{ids:ids,sureDel:flag},
        async:false,
        success:function(data){
            window.top.layer.msg(data.msg,{icon:6,offset: 'rb',area:['120px','80px'],anim:2});
        },error:function(){
            alert('error');
        }
    });
}
/*
 title   标题
 url     请求的url
 id      需要操作的数据id
 w       弹出层宽度（缺省调默认值）
 h       弹出层高度（缺省调默认值）
 */
function layerSave(title, url, w, h) {
    if (title == null || title == '') {
        title = false;
    }
    if (url == null || url == '') {
        url = "404.html";
    }
    if (w == null || w == '') {
        w = ($(window).width() * 0.9);
    }
    if (h == null || h == '') {
        h = ($(window).height() - 50);
    }
    layer.open({
        id: 'layer-save',
        type: 2,
        area: [w + 'px', h + 'px'],
        fix: false,
        maxmin: true,
        shadeClose: false,
        shade: 0.4,
        title: title,
        content: url + '&detail=false'
    });
}
function layerDetail(title, url, w, h) {
    var number = 1;
    if (title == null || title == '') {
        title = false;
    };
    if (url == null || url == '') {
        url = "404.html";
    };
    if (w == null || w == '') {
        w = ($(window).width() * 0.9);
    };
    if (h == null || h == '') {
        h = ($(window).height() - 50);
    };
    layer.open({
        id: 'layer-detail',
        type: 2,
        area: [w + 'px', h + 'px'],
        fix: false,
        maxmin: true,
        shadeClose: true,
        shade: 0.4,
        title: title,
        content: url + '&detail=true',
        // btn:['关闭']
    });
}
function layerSimpleOpen(title,url,w,h){
    if (title == null || title == '') {
        title = false;
    };
    if (url == null || url == '') {
        url = "404.html";
    };
    if (w == null || w == '') {
        w = ($(window).width() * 0.9);
    };
    if (h == null || h == '') {
        h = ($(window).height() - 50);
    };
    layer.open({
        id: 'layer-simpleOpen',
        type: 2,
        area: [w + 'px', h + 'px'],
        fix: false,
        maxmin: true,
        shadeClose: true,
        shade: 0.4,
        title: title,
        content: url,
    });
}
</script>