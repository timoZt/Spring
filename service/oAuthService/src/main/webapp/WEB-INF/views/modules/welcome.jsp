<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>我的桌面</title>
</head>
<body>
<section class="container-fluid page-404 minWP text-c">
  <p class="error-title" style="font-size:400%;">欢迎使用后台管理系统</p>
  <div style="width: 20%">
    <div id="img"></div>
    <input type="hidden" name="url">
    <input type="hidden" name="fileId">
  </div>
</section>
<script type="application/javascript">
    var img=  $("#img").diyUpload({
        url:oss_url,
        multiple:false,
        imgTop:true,
        auto:true,
        /*startUpButton:true,*/
        oss:true,
       /* chunked:true,
        chunkRetry:10,
        chunkSize:15 * 1024 * 1024,*/
        buttonText:"上传图片",
        accept:{
           /* title:"Images",
            extensions:"jpg,png",*/
            mimeTypes:"video/*"
        },
        success:function(imgInfo,data){
            console.log(imgInfo);
            console.log(data);
/*
            {"msg":"成功","flag":200,"data":"http://qssd.oss-cn-shenzhen.aliyuncs.com/upload/8NXRDQZe5sbQJbceTrjh.MP4?Expires=1613612309&OSSAccessKeyId=LTAIyv7OuA7frgyi&Signature=6ud7h10M01ipRv8tLaL9ujvTYfc%3D","isUpdate":"true"}
*/
            if(data&&data.flag==200){
                $("#img").hide();
                $("#saveForm input[name=url]").val(data.data.dataPath);
                $("#saveForm input[name=fileId]").val(data.data.id);
            }
        }
    });

</script>
</body>
</html>