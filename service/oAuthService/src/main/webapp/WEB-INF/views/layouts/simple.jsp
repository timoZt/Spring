<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html style="overflow-x:auto;overflow-y:auto;">
<head>
	<!-- <title><sitemesh:write property='title' /></title> -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=8,IE=9,IE=10" />
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-store">
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>


	<link rel="stylesheet" type="text/css" href="${ctxStatic}/self/css/style.css" />
	<link rel="stylesheet" href="${ctxStatic}/kitadmin/plugins/layui/css/layui.css">
	<script type="text/javascript" src="${ctxStatic}/thirdparty/jquery/1.9.1/jquery.js"></script>
	<sitemesh:write property='head'/>

	<script type="text/javascript" src="${ctxStatic}/self/jeesite.js"></script>
	<script type="text/javascript" src="${ctxStatic}/self/js/common.js"></script>

	<%--ztree--%>
	<link href="${ctxStatic}/thirdparty/zTree/3.5.12/css/zTreeStyle/zTreeStyle.min.css" rel="stylesheet" type="text/css"/>
	<script src="${ctxStatic}/thirdparty/zTree/3.5.12/js/jquery.ztree.all-3.5.min.js" type="text/javascript"></script>
	<script src="${ctxStatic}/thirdparty/zTree/3.5.12/js/jquery.ztree.exhide-3.5.js" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" href="${ctxStatic}/self/tree/alertZtreeCss.css" >
	<script type="text/javascript" src="${ctxStatic }/self/tree/zTree.js"></script>
</head>
<body>
	<sitemesh:write property='body'/>
	<sitemesh:write property='myfooter'/>
</body>
</html>