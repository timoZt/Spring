<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html style="overflow-x:auto;overflow-y:auto;">
<head>
	<!-- <title><sitemesh:write property='title' /></title> -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=8,IE=9,IE=10" />
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-store">

	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>

	<title>${fns:getConfig('productFullName')}</title>
	<LINK rel="Bookmark" href="${ctxStatic }/yuansheng.ico" />
	<LINK rel="Shortcut Icon" href="${ctxStatic }/yuansheng.ico" />
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<sitemesh:write property='head'/>
</head>
<body>
	<sitemesh:write property='body'/>
	<%@include file="/WEB-INF/views/include/body.jsp" %>
	<sitemesh:write property='myfooter'/>
</body>
</html>