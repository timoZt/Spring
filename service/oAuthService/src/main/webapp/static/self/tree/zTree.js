/**
 * input单击弹出树形选择
 * @param inputId  input的id
 * @param dialogId   input所在弹出框的id
 */
var outPutDiv;
function showMenu(inputId,upOrDown){
	outPutDiv="_"+inputId;
	var obj = $("#"+inputId);
	var offset = $("#"+inputId).offset();
	if(upOrDown){
		/*弹出在上面*/
		var offsetOfDialog =$("#"+inputId).parents('.layui-layer').offset();
		var offsetOfDialogTitle = $("#"+inputId).parents('.layui-layer').find(".layui-layer-title").outerHeight();
		$("#menuContent"+outPutDiv).css({top:offset.top-offsetOfDialog.top-offsetOfDialogTitle-150+ "px"});
	}
	$("#menuContent"+outPutDiv).slideDown("fast");
	$("body").bind("mousedown", onBodyDown2);
}
function hideMenu2() {
	$("#menuContent"+outPutDiv).fadeOut("fast");
	$("body").unbind("mousedown", onBodyDown2);
}
function onBodyDown2(event) {
	if (!(event.target.id == "menuBtn" || event.target.id == "menuContent"+outPutDiv || $(event.target).parents("#menuContent"+outPutDiv).length>0)) {
		hideMenu2();
	}
} 

/****************************我是分割线******************************************/
/**
 * 获取多选的id的json串
 * @param obj 树id
 * @returns {String}  
 */
function getTreeCheckboxIdCheck(obj){
	var treeObj = $.fn.zTree.getZTreeObj(obj);
	var nodes = treeObj.getCheckedNodes(true);
	if(nodes.length==0){
		return null;
	}
	/*var	json={};
	json.id=[];*/
	var ids =[];
	for(var i = 0 ;i<nodes.length;i++){
		ids.push(nodes[i].id);
	}
	return ids.join(",");
}
/**
 * 获取多选的id
 * @param treeId 树id
 * @returns Array
 */
function getTreeCheckboxIdCheckIds(treeId){
	var ids = [];
	var treeObj = $.fn.zTree.getZTreeObj(treeId);
	var nodes = treeObj.getCheckedNodes(true);
	if(nodes.length==0){
		return ids;
	}
	for(var i = 0 ;i<nodes.length;i++){
		ids.push(nodes[i].id);
	}
	return ids;
}
function getTreeCheckboxNameCheck(obj){
	var treeObj = $.fn.zTree.getZTreeObj(obj);
	var nodes = treeObj.getCheckedNodes(true);
	if(nodes.length==0){
		return null;
	}
	var names =[];
	for(var i = 0 ;i<nodes.length;i++){
		names.push(nodes[i].name);
		/*	names+=nodes[i].name;*/
		/*if(i!=nodes.length-1){
			names+=",";
		}*/
	}
	return names;
}
/**
 * 获取最后选中的name值--只针对单选-本项目中为学生选班级
 * @param obj
 * @returns
 */
function getTreeCheckboxNameCheckLast(obj){
	var treeObj = $.fn.zTree.getZTreeObj(obj);
	var nodes = treeObj.getCheckedNodes(true);
	if(nodes.length==0){
		return null;
	}
	var names =[];
	for(var i = nodes.length-1 ;i<nodes.length;i++){
			names.push(nodes[i].name);
	}
	return names;
}



/**单选:取消其子项除第一个以外的级联选中**/
function checkSonCheck(treeNode){
	var nodes	= treeNode.children;
	var checked = false;
	$.each(nodes,function(i,n){
		if(checked){
			n.checked=false;
			if(n.children!=null){
				cancelSonCheck(n);
			}
		}else{
			/**第一次循环，第一个子节点**/
			checked=true;
			if(n.children!=null){
				checkSonCheck(n);
			}
		}
	});
}
/**单选:取消同级子项级联选中**/
function cancelSonCheck(treeNode){
	var nodes	= treeNode.children;
	$.each(nodes,function(i,n){
		n.checked=false;
		if(n.children!=null){
			cancelSonCheck(n);
		}
	});
}
