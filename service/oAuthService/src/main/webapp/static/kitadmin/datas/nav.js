var navs = [ {
    "title": "系统管理",
    "icon": "fa-address-book",
    "href": "",
    "spread": true,
    "children": [{
        "title": "用户管理",
        "icon": "fa-qq",
        "href": "a/pageSkip/jumpPage?pageUrl=system/user/userList"
    }, {
        "title": "菜单管理",
        "icon": "&#xe609;",
        "href": "a/pageSkip/jumpPage?pageUrl=system/resource/resourceList"
    }, {
        "title": "字典管理",
        "icon": "&#xe609;",
        "href": "http://fly.layui.com/"
    }, {
        "title": "系统参数",
        "icon": "fa-weibo",
        "href": "http://weibo.com/"
    },{
        "title": "常见问题",
        "icon": "fa-github",
        "href": "https://www.github.com/"
    },{
        "title": "反馈标签",
        "icon": "fa-github",
        "href": "https://www.github.com/"
    },{
        "title": "意见反馈",
        "icon": "fa-github",
        "href": "https://www.github.com/"
    },{
        "title": "系统日志",
        "icon": "fa-github",
        "href": "https://www.github.com/"
    },{
        "title": "公告消息",
        "icon": "fa-github",
        "href": "https://www.github.com/"
    }]
},{
	"title": "基本元素",
	"icon": "fa-cubes",
	"spread": false,
	"children": [{
		"title": "按钮",
		"icon": "&#xe641;",
		"href": "a/pageSkip/jumpPage?pageUrl=layui/button"
	}, {
		"title": "表单",
		"icon": "&#xe63c;",
		"href": "a/pageSkip/jumpPage?pageUrl=layui/form"
	}, {
		"title": "表格",
		"icon": "&#xe63c;",
		"href": "a/pageSkip/jumpPage?pageUrl=layui/table"
	}, {
		"title": "导航",
		"icon": "&#xe609;",
		"href": "a/pageSkip/jumpPage?pageUrl=layui/nav"
	}, {
		"title": "辅助性元素",
		"icon": "&#xe60c;",
		"href": "a/pageSkip/jumpPage?pageUrl=layui/auxiliar"
	}]
}, {
	"title": "组件",
	"icon": "fa-cogs",
	"spread": false,
	"children": [{
		"title": "BTable",
		"icon": "fa-table",
		"href": "a/pageSkip/jumpPage?pageUrl=layui/btable"
	}, {
		"title": "Navbar组件",
		"icon": "fa-navicon",
		"href": "a/pageSkip/jumpPage?pageUrl=layui/navbar"
	}, {
		"title": "Tab组件",
		"icon": "&#xe62a;",
		"href": "a/pageSkip/jumpPage?pageUrl=layui/tab"
	}, {
		"title": "Laytpl+Laypage",
		"icon": "&#xe628;",
		"href": "a/pageSkip/jumpPage?pageUrl=layui/paging"
	}]
}, {
	"title": "第三方组件",
	"icon": "&#x1002;",
	"spread": false,
	"children": [{
		"title": "iCheck组件",
		"icon": "fa-check-square-o",
		"href": "a/pageSkip/jumpPage?pageUrl=layui/icheck"
	}]
}, {
	"title": "这是一级导航",
	"icon": "fa-stop-circle",
	"href": "https://www.baidu.com",
	"spread": false
}, {
	"title": "其他",
	"icon": "fa-stop-circle",
	"href": "#",
	"spread": false,
	"children": [{
		"title": "子窗体中打开选项卡",
		"icon": "fa-github",
		"href": "cop.html"
	}]
}];