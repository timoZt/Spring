/* 
*	jQuery文件上传插件,封装UI,上传处理操作采用Baidu WebUploader;
*  @Author 黑爪爪;
*  @edit timo
*/
(function( $ ) {
	var  uploadFileNumberTopAce = 0;
	var  fileIds = [];
    $.fn.extend({
		/*
		*	上传方法 opt为参数配置;
		*	serverCallBack回调函数 每个文件上传至服务端后,服务端返回参数,无论成功失败都会调用 参数为服务器返回信息;
		*   success 上传成功回掉方法 successCallBack( file,response );  返回响应和文件
		*   error  上传错误回掉方法 errorCallBack( err ); 返回队伍信息
		*   multiple 是否支持多文件上传--在导入配置以后并没有删除属性
		*   buttonText 按钮文字--不传入便没有选择文件按钮
		*   startUpload 上传回调事件
		*   startUpButton 是否开启全部上传按钮
		*/
        diyUpload:function( opt, serverCallBack ) {
 			if ( typeof opt != "object" ) {
				layer.alert('参数错误!',{icon:5});
				return;	
			}
			var $fileInput = $(this);
			var $fileInputId = $fileInput.attr('id');
			//组装参数;
			if( opt.url ) {
				opt.server = opt.url; 
				delete opt.url;
			}
            if( opt.chunkSize ) {
                var chunkSize = opt.chunkSize;
                delete opt.chunkSize;
            }
            if( opt.chunkRetry ) {
                var chunkRetry = opt.chunkRetry;
                delete opt.chunkRetry;
            }
            if( opt.chunked ) {
                var chunked = opt.chunked;
                delete opt.chunked;
            }
			if( opt.success ) {
				var successCallBack = opt.success;
				delete opt.success;
			}
			if(opt.uploadFinished){
				var uploadFinished = opt.uploadFinished;
				delete opt.uploadFinished;
			}
			if( opt.error ) {
				var errorCallBack = opt.error;
				delete opt.error;
			}
			if(opt.startUpload){
				var startUpload=opt.startUpload;
				delete opt.startUpload;
			}
			//迭代出默认配置
			$.each( getOption( '#'+$fileInputId ),function( key, value ){
					opt[ key ] = opt[ key ] || value; 
			});
			if(opt.multiple!=null){
				opt.pick.multiple=opt.multiple;
				//这里不做删除
			}
			if (opt.buttonText ) {
				opt['pick']['id'] =  $fileInput;
				opt['pick']['label'] = opt.buttonText;
				delete opt.buttonText;	
			}else{
				opt['pick']['id'] =  $fileInput;
				$fileInput.hide();
			}
			var webUploader = getUploader( opt );
			if ( !WebUploader.Uploader.support() ) {
				layer.alert( ' 上传组件不支持您的浏览器！',{icon:5});
				return false;
       		}	
			if(startUpload){
				webUploader.on('startUpload',function(file){
					startUpload();
				});
			}
			if($("#uploadFileNumber")){//如果使用的是Ace前段框架
				webUploader.on('uploadStart', function( file ) {
					//hasOwnProperty是用来判断一个对象是否有你给出名称的属性或对象
					//不过需要注意的是，此方法无法检查该对象的原型链中是否具有该属性，该属性必须是对象本身的一个成员。
					if(!fileIds.hasOwnProperty(file.id)){
						//uploadFileNumber和uploadFileNumberTop ace前段框架中自定义的字段--还有多少文件在上传
						$("#uploadFileNumber").text(parseInt($("#uploadFileNumber").text())+1);
						$("#uploadFileNumberTop").text(parseInt($("#uploadFileNumberTop").text())+1);
						fileIds.push(file.id);
						var div = 
							'<li>'
							+ '<a href="#">'
								+ '<div class="clearfix">'
									+ '<span class="pull-left">'+file.name+'</span> <span class="pull-right" id="'+file.id+'_file">0%</span>'
								+ '</div>'
								+ '<div class="progress progress-mini ">'
									+ '<div style="width:0%" class="progress-bar " id="'+file.id+'_fileWidth"></div>'
								+ '</div>'
							+ '</a>'
						+ '</li>';
						$("#uploadFile").after(div);
					}
				});
			}
			//绑定文件加入队列事件;
			webUploader.on('fileQueued', function( file ) {
				createBox( $fileInput, file ,webUploader,opt);
			});
			//当文件被加入队列之前触发，此事件的handler返回值为false，则此文件不会被添加进入队列。
			webUploader.on('beforeFileQueued', function( file ) {
				if(opt.multiple!=null && !opt.multiple){
					//单选删除原本的文件
					var fileArr = webUploader.getFiles( 'queued' );
					$.each( fileArr ,function( i, v ){
						removeLi( $('#fileBox_'+v.id), v.id, webUploader );
					});
					webUploader.reset();//重置队列
					if(opt.imgTop){
						$fileInput.prev('.parentFileBox').remove();
					}else{
						$fileInput.next('.parentFileBox').remove();
					}
				}
			});
			//进度条事件
			webUploader.on('uploadProgress',function( file, percentage  ){
				var $fileBox = $('#fileBox_'+file.id);
				var $diyBar = $fileBox.find('.diyBar');	
				$diyBar.show();
				percentage = percentage*100;
				showDiyProgress( percentage.toFixed(2), $diyBar);
				
				var $diyBar1 = $('#'+file.id+'_file');
				var $diyBar2 = $('#'+file.id+'_fileWidth');
				showDiyProgressOfAce(percentage.toFixed(2), $diyBar1);
				showDiyProgressOfAceWidth( percentage.toFixed(2), $diyBar2);
			});
			
			//全部上传结束后触发;
			webUploader.on('uploadFinished', function(){
				$fileInput.next('.parentFileBox').children('.diyButton').remove();
				if(uploadFinished){
					uploadFinished();
				}
			});
			//绑定发送至服务端返回后触发事件;
			webUploader.on('uploadAccept', function( object ,data ){
				if ( serverCallBack ) serverCallBack( data );
			});
			
			//上传成功后触发事件;
			webUploader.on('uploadSuccess',function( file, response ){
				var $fileBox = $('#fileBox_'+file.id);
				var $diyBar = $fileBox.find('.diyBar');	
				$fileBox.removeClass('diyUploadHover');
				$diyBar.fadeOut( 1000 ,function(){
					$fileBox.children('.diySuccess').show();
				});
				if ( successCallBack ) {
					successCallBack( file,response );
				}	
			});
			
			//上传失败后触发事件;
			webUploader.on('uploadError',function( file, reason ){
				var $fileBox = $('#fileBox_'+file.id);
				var $diyBar = $fileBox.find('.diyBar');	
				showDiyProgress( 0, $diyBar , '上传失败!' );
				
				var $diyBar1 = $('#'+file.id+'_file');
				showDiyProgressOfAce(0, $diyBar1, '上传失败!');
				
				var err = '上传失败! 文件:'+file.name+' 错误码:'+reason;
				if ( errorCallBack ) {
					errorCallBack( err );
				}
			});
			
			//错误触发事件;
			webUploader.on('error', function( code ) {
				var text = '';
				switch( code ) {
					case  'F_DUPLICATE' : text = '该文件已经被选择了!' ;
					break;
					case  'Q_EXCEED_NUM_LIMIT' : text = '上传文件数量超过限制!' ;
					break;
					case  'F_EXCEED_SIZE' : text = '文件大小超过限制!';
					break;
					case  'Q_EXCEED_SIZE_LIMIT' : text = '所有文件总大小超过限制!';
					break;
					case 'Q_TYPE_DENIED' : text = '文件类型不正确或者是空文件!';
					break;
					default : text = '未知错误!';
 					break;	
				}
            	layer.alert( text,{icon:5});
        	});
			return webUploader;
        }
    });
	
	//Web Uploader默认配置;
	function getOption(objId) {
		/*
		*	配置文件同webUploader一致,这里只给出默认配置.
		*	具体参照:http://fex.baidu.com/webuploader/doc/index.html
		*/
		return {
			//按钮容器;
			pick:{
//				id:objId
//				label:"点击选择文件"
			},
			//类型限制;
			accept:{
				title:"Images",
				extensions:"gif,jpg,jpeg,bmp,png",
				mimeTypes:"image/*"
			},
			//配置生成缩略图的选项
			thumb:{
				width:170,
				height:150,
				// 图片质量，只有type为`image/jpeg`的时候才有效。
				quality:70,
				// 是否允许放大，如果想要生成小图的时候不失真，此选项应该设置为false.
				allowMagnify:false,
				// 是否允许裁剪。
				crop:true,
				// 为空的话则保留原有图片格式。
				// 否则强制转换成指定的类型。
				type:"image/jpeg"
			},
			//文件上传方式
			method:"POST",
			//服务器地址;
			server:"",
			//是否已二进制的流的方式发送文件，这样整个上传内容php://input都为文件内容
			sendAsBinary:false/*,
			// 开起分片上传。 thinkphp的上传类测试分片无效,图片丢失;
			chunked:true,
			// 分片大小
			chunkSize:512 * 1024,
			//最大上传的文件数量, 总文件大小,单个文件大小(单位字节);
			fileNumLimit:50,
			fileSizeLimit:5000 * 1024,
			fileSingleSizeLimit:500 * 1024*/
		};
	}
	
	//实例化Web Uploader
	function getUploader( opt ) {
		return new WebUploader.Uploader( opt );;
	}
	
	//操作进度条;
	function showDiyProgress( progress, $diyBar, text ) {
		if ( progress >= 100 ) {
			progress = progress + '%';
			text = text || '上传完成';
		} else {
			progress = progress + '%';
			text = text || progress;
		}
		var $diyProgress = $diyBar.find('.diyProgress');
		var $diyProgressText = $diyBar.find('.diyProgressText');
		$diyProgress.width( progress );
		$diyProgressText.text( text );
	}
	//操作进度条;
	function showDiyProgressOfAceWidth( progress, $diyBar ) {
		progress = progress + '%';
		$diyBar.width( progress );
	}
	//操作进度条;
	function showDiyProgressOfAce( progress, $diyBar,text1) {
		if ( progress == 100 ) {
			progress = '上传完成';
			
			//防止第二次--精度造成的问题
			if(uploadFileNumberTopAce==0){
				uploadFileNumberTopAce=1;
				$("#uploadFileNumber").text(parseInt($("#uploadFileNumber").text())-1);
				$("#uploadFileNumberTop").text(parseInt($("#uploadFileNumberTop").text())-1);
			}else if(uploadFileNumberTopAce==1){
				uploadFileNumberTopAce=0;
			}
		} else {
			progress = progress + '%';
		}
		if(text1!=null && text1!=""){
			$diyBar.text( text1 );
		}else{
			$diyBar.text( progress );
		}
	}
	
	//取消事件;	
	function removeLi ( $li ,file_id ,webUploader) {
		webUploader.removeFile( file_id );
		if ( $li.siblings('li').length <= 0 ) {
			$li.parents('.parentFileBox').remove();
		} else {
			$li.remove();
		}
		
	}
	
	//创建文件操作div;	
	function createBox( $fileInput, file, webUploader,opt) {
		if(opt.imgTop){
			$fileInput.prev('.updateUploadImg').remove();//删除修改时显示在原本位置的图片
		}else{//如果没有值的话默认图片在上方，这此行代码亦无效
			$fileInput.next('.updateUploadImg').remove();//删除修改时显示在原本位置的图片
		}
		var  queue= $fileInput.parent().next('.filelist');
		if(opt.fileContent){
			if ( queue.length <= 0 ) {
				queue = $('<ul class="filelist"></ul>').appendTo( $("#"+opt.fileContent).find('.queueList') );
			}
		}
		var file_id = file.id;
		if( opt.fileContent ){//判断是否使用了文件容器
			$wrap = $('#'+opt.fileContent);
			
			$statusBar = $wrap.find('.statusBar');
		    $statusBar.removeClass('element-invisible');
            $statusBar.show();
            
			$placeHolder = $wrap.find('.placeholder');
			$placeHolder.addClass( 'element-invisible' );
			
            $( '#filePicker2' ).removeClass( 'element-invisible');
            queue.parent().addClass('filled');
            queue.show();
        
            webUploader.refresh();
            webUploader.addButton({
                id: '#filePicker2',
                label: '添加资源'
            });
			 var $li = $( '<li id="' + file.id + '">' +
		                '<p class="title">' + file.name + '</p>' +
		                '<p class="imgWrap"></p>'+
		                '<p class="progress"><span></span></p>' +
		                '</li>' ),
		            $btns = $('<div class="file-panel">' +
		                '<span class="cancel">删除</span>' +
		                '<span class="down">下移</span>' +
		                '<span class="up">上移</span>' 
		                /* '<span class="rotateRight">向右旋转</span>' +
		                '<span class="rotateLeft">向左旋转</span></div>'*/).appendTo( $li ),
		            $prgress = $li.find('p.progress span'),
		            $wrap = $li.find( 'p.imgWrap' ),//文件p
		            $info = $('<p class="error"></p>'),
		            showError = function( code ) {
		                switch( code ) {
		                    case 'exceed_size':
		                        text = '文件大小超出';
		                        break;
		                    case 'interrupt':
		                        text = '上传暂停';
		                        break;
		                    default:
		                        text = '上传失败，请重试';
		                        break;
		                }
		                $info.text( text ).appendTo( $li );
		            };
		        //鼠标经过露出图片上的按钮
		        $li.on( 'mouseenter', function() {
		        	if ( file.type.split("/")[0] != 'image' ) {
		        		$btns.stop().animate({height:330});
		        	}else{
		        		$btns.stop().animate({height:30});
		        	}
		        });
		        $li.on( 'mouseleave', function() {
		            $btns.stop().animate({height: 0});
		        });
		        //按钮操作事件
		        $btns.on('click','span',function() {
		            var index = $(this).index(),//span的位置
		            deg;
		            switch ( index ) {
		                case 0:
		                	webUploader.removeFile(file);
	                        var $li = $('#'+file.id);
	                        $li.off().find('.file-panel').off().end().remove();
	                        opt.deleteFile(file);
		                    return;
		                case 1:
		                	var $li = $('#'+file.id);
		                	$li.insertAfter($li.next());
		                    break;
		                case 2:
		                	var $li = $('#'+file.id);
		                	$li.insertBefore($li.prev());	
		                    break;
		              /*  case 1:
		                    file.rotation += 90;
		                    break;
		                case 2:
		                    file.rotation -= 90;
		                    break;*/
		            }
//		            if (supportTransition) {//改变图片的css值--在旋转的时候
		                /*deg = 'rotate(' + file.rotation + 'deg)';
		                $wrap.css({
		                    '-webkit-transform': deg,
		                    '-mos-transform': deg,
		                    '-o-transform': deg,
		                    'transform': deg
		                });*/
//		            } else {
//		                $wrap.css( 'filter', 'progid:DXImageTransform.Microsoft.BasicImage(rotation='+ (~~((file.rotation/90)%4 + 4)%4) +')');
//		            }
		        });
		        $li.appendTo( queue );

		        if ( file.getStatus() === 'invalid' ) {
		            showError( file.statusText );
		        } else {
//		            $wrap.text( '预览中' );
		           /* file.rotation = 0;*/
		        	//判断文件的后缀-然后选定对应的图片作为背景图 $fileBox为文件li
					if ( file.type.split("/")[0] != 'image' ) {
						$wrap.parents("#"+file.id).addClass("file_diy_fileContent_video");
//						webUploader.makeThumb( f, function( error, src ) {//生成缩略图
//			                var img = $('<img src="'+src+'">');
//			                $wrap.empty().append( img );
//			            }, 480, 310 );
					}else{
						//生成预览缩略图;
						webUploader.makeThumb( file, function( error, dataSrc ) {
							if ( !error ) {	
									$wrap.append('<img src="'+dataSrc+'" onclick="divUploadImgOnClick(this);">');
							}
						});
					}
//		            percentages[ file.id ] = [ file.size, 0 ];
		        }
		}else{
			var $parentFileBox;
			if(opt.imgTop){
				$parentFileBox = $fileInput.prev('.parentFileBox');
			}else{
				$parentFileBox = $fileInput.next('.parentFileBox');
			}
			//添加父系容器;
			if ( $parentFileBox.length <= 0 ) {
				var div = '<div class="parentFileBox"> \
							<ul class="fileBoxUl"></ul>\
						</div>';
				if(opt.imgTop){
					$fileInput.before(div);
					$parentFileBox = $fileInput.prev('.parentFileBox');
				}else{
					$fileInput.after( div );
					$parentFileBox = $fileInput.next('.parentFileBox');
				}
			}
			//创建按钮
			if ( $parentFileBox.find('.diyButton').length <= 0 && opt.startUpButton ) {
				var div = '<div class="diyButton"> \
							<a class="diyStart" href="javascript:void(0)">开始上传</a> \
							<a class="diyCancelAll" href="javascript:void(0)">全部取消</a> \
						</div>';
				$parentFileBox.append( div );
				var $startButton = $parentFileBox.find('.diyStart');
				var $cancelButton = $parentFileBox.find('.diyCancelAll');
				//开始上传,暂停上传,重新上传事件;
				var uploadStart = function (){
					webUploader.upload();
					/*$startButton.text('暂停上传').one('click',function(){
							webUploader.stop();
							$(this).text('继续上传').one('click',function(){
									uploadStart();
							});
					});*/
				}
				//绑定开始上传按钮;
				$startButton.one('click',uploadStart);
				//绑定取消全部按钮;
				$cancelButton.bind('click',function(){
					var fileArr = webUploader.getFiles( 'queued' );
					$.each( fileArr ,function( i, v ){
						removeLi( $('#fileBox_'+v.id), v.id, webUploader );
					});
				});
			}
				
			//添加子容器;
			var li = '<li id="fileBox_'+file_id+'" class="diyUploadHover"> \
						<div class="viewThumb"></div> \
						<div class="diyCancel"></div> \
						<div class="diySuccess"></div> \
						<div class="diyFileName">'+file.name+'</div>\
						<div class="diyBar"> \
								<div class="diyProgress"></div> \
								<div class="diyProgressText">0%</div> \
						</div> \
					</li>';
			$parentFileBox.children('.fileBoxUl').append( li );
			//父容器宽度;
			var $width = $('.fileBoxUl>li').length * 200;
			var $maxWidth = $fileInput.parent().width();
			$width = $maxWidth > $width ? $width : $maxWidth;
			$parentFileBox.width( $width );
			
			var $fileBox = $parentFileBox.find('#fileBox_'+file_id);

			//绑定取消事件;
			var $diyCancel = $fileBox.children('.diyCancel').one('click',function(){
				removeLi( $(this).parent('li'), file_id, webUploader );	
			});
			//判断文件的后缀-然后选定对应的图片作为背景图 $fileBox为文件li
			if ( file.type.split("/")[0] != 'image' ) {
				var liClassName = getFileTypeClassName( file.name.split(".").pop() );
				$fileBox.addClass(liClassName);
				$fileBox.find('.viewThumb').attr("onclick","divUploadImgOnClick(this);");
				return;	
			}
			//生成预览缩略图;
			webUploader.makeThumb( file, function( error, dataSrc ) {
				if ( !error ) {	
					//如果是图片
//					if(opt.accept){
					if(opt.multiple!=null && !opt.multiple){//单选
						$fileBox.find('.viewThumb').append('<img src="'+dataSrc+'" onclick="divUploadImgOnClick(this);">');
					}else{
						$fileBox.find('.viewThumb').append('<img src="'+dataSrc+'" >');
					}
//					}else{
//						$fileBox.find('.viewThumb').append('<img src="'+dataSrc+'">');
//					}
				}
			});
		}		
	}
	//获取文件类型;
	function getFileTypeClassName ( type ) {
		var fileType = {};
		var suffix = '_diy_bg';
		fileType['pdf'] = 'pdf';
		fileType['zip'] = 'zip';
		fileType['rar'] = 'rar';
		fileType['csv'] = 'csv';
		fileType['doc'] = 'doc';
		fileType['xls'] = 'xls';
		fileType['xlsx'] = 'xls';
		fileType['txt'] = 'txt';
		
		fileType['flv'] = 'flv';
		fileType['mp4'] = 'mp4';
		fileType['apk'] = 'apk';
		
		fileType = fileType[type] || 'file';
		return 	fileType+suffix;
	}
	
	$.fn.extend({
		//修改时原本的图片--普通上传-
		//dataSrc图片路径-
		//deleteFun图片删除回掉方法,返回了图片路径-
		//imgTop图片位置，值为true上false下
	    updateUploadImg:function(dataSrc,imgTop,deleteFun){
	    	//updateUploadImg-标注是修改的图片div-在创建新的div的时候会做删除
			var div = '<div class="parentFileBox  updateUploadImg"> \
								<ul class="fileBoxUl"></ul>\
							</div>';
			if(imgTop){
				$(this).before(div);
				$parentFileBox = $(this).prev('.parentFileBox');
			}else{
				$(this).after(div);
				$parentFileBox = $(this).next('.parentFileBox');
			}
			//添加子容器;
			var li = '<li id="fileBox_" class="diyUploadHover"> \
							<div class="viewThumb" id="viewThumb"></div> \
							<div class="diyCancel" ></div> \
							<div class="diySuccess"></div> \
							<div class="diyBar"> \
									<div class="diyProgress"></div> \
									<div class="diyProgressText">0%</div> \
							</div> \
						</li>';
			$parentFileBox.children('.fileBoxUl').append( li );
			var $fileBox = $parentFileBox.find('#fileBox_');
			$fileBox.find('.viewThumb').append('<img src="'+dataSrc+'" onclick="divUploadImgOnClick(this);">');
			if(deleteFun){
				$fileBox.find(".diyCancel").bind("click",function(){
					var  dataUrl = $(this).prev().find("img").attr("src");
					deleteFun(dataUrl);
					$(this).parents(".diyUploadHover").remove();
				});
			}else{
				$fileBox.find(".diyCancel").remove();
			}
		}
	});
	
	$.fn.extend({
		//修改时原本的图片集合  
		//dataSrc是一个路径数组,
		//deleteFun图片删除回掉方法,返回了图片路径-
		//imgTop图片位置，值为true上false下
	    updateUploadImgs:function(dataSrc,imgTop,deleteFun){
			var div = '<div class="parentFileBox "> \
								<ul class="fileBoxUl"></ul>\
							</div>';
			if(imgTop){
				$(this).before(div);
				$parentFileBox = $(this).prev('.parentFileBox');
			}else{
				$(this).after(div);
				$parentFileBox = $(this).next('.parentFileBox');
			}
			for(var i =0 ; i<dataSrc.length;i++){
				//添加子容器;
				var li = '<li id="fileBox_'+i+'" class="diyUploadHover"> \
								<div class="viewThumb"></div> \
								<div class="diyCancel" ></div> \
								<div class="diySuccess"></div> \
								<div class="diyBar"> \
										<div class="diyProgress"></div> \
										<div class="diyProgressText">0%</div> \
								</div> \
							</li>';
				$parentFileBox.children('.fileBoxUl').append( li );
				var $fileBox = $parentFileBox.find('#fileBox_'+i);
				$fileBox.find('.viewThumb').append('<img src="'+dataSrc[i]+'"  >');
				if(deleteFun){
					$fileBox.find(".diyCancel").bind("click",function(){
						var  dataUrl = $(this).prev().find("img").attr("src");
						deleteFun(dataUrl);
						$(this).parents(".diyUploadHover").remove();
					});
				}else{
					$fileBox.find(".diyCancel").remove();
				}
			}
		}
	});
	
	$.fn.extend({
		//fileContent 上传容器
		//dataSrc 图片路径或资源路径  fileId 文件id  webUploader 上传对象  text 文字   deleteFun 删除回掉
		//修改时原本的图片--作品上传
	    updateUploadOpus:function(fileContent,dataSrc,fileId,webUploader,text,deleteFun){
	    	var  queue= this.parent().next('.filelist');
			if ( queue.length <= 0 ) {
				queue = $('<ul class="filelist"></ul>').appendTo( $("#"+fileContent).find('.queueList') );
				$wrap = $('#'+fileContent);
				$statusBar = $wrap.find('.statusBar');
			    $statusBar.removeClass('element-invisible');
	            $statusBar.show();
				$placeHolder = $wrap.find('.placeholder');
				$placeHolder.addClass( 'element-invisible' );
	            $( '#filePicker2' ).removeClass( 'element-invisible');
	            queue.parent().addClass('filled');
	            queue.show();
	            webUploader.refresh();
	            webUploader.addButton({
	                id: '#filePicker2',
	                label: '添加资源'
	            });
			}
			if(text==""){
				  var $li = $( '<li id='+fileId+'>' +
			                '<p class="title"></p>' +
			                '<p class="imgWrap"></p>'+
			                '<p class="progress"><span></span></p>' +
			                '</li>' ),
			            $btns = $('<div class="file-panel">' +
			                '<span class="cancel">删除</span>' ).appendTo( $li ),
			            $prgress = $li.find('p.progress span'),
			            $wrap = $li.find( 'p.imgWrap' ),//文件p
			            $info = $('<p class="error"></p>');
			        //鼠标经过露出图片上的按钮
			        $li.on( 'mouseenter', function() {
			        	if (( dataSrc.split(".")[1] != 'png' ) && (dataSrc.split(".")[1] != 'jpg')) {
			        		$btns.stop().animate({height:330});
			        	}else{
			        		$btns.stop().animate({height:30});
			        	}
			        });
			        $li.on( 'mouseleave', function() {
			            $btns.stop().animate({height: 0});
			        });
			        //按钮操作事件
			        $btns.on('click','span',function() {
			            var index = $(this).index(),//span的位置
			            deg;
			            switch ( index ) {
			                case 0:
		                        var $li = $('#'+fileId);
		                        $li.off().find('.file-panel').off().end().remove();
		                        deleteFun(fileId);
			                    return;
			            }
			        });
			        $li.appendTo( queue );
			        if (( dataSrc.split(".")[1] != 'png' ) && (dataSrc.split(".")[1] != 'jpg')) {
						$wrap.parents("#"+fileId).addClass("file_diy_fileContent_video");
					}else{
						$wrap.append('<img src="'+dataSrc+'" onclick="divUploadImgOnClick(this);">');
					}
			}else{
				$("#"+fileContent).find("ul").append("<p>"+text+"</p>");
			}
		}
	});
	$.fn.extend({
		//fileContent 上传容器
		//dataSrc 图片路径或资源路径  fileId 文件id  webUploader 上传对象  text 文字   deleteFun 删除回掉
		//修改时原本的图片--作品上传
	    updateUploadFileContent:function(fileContent,dataSrc,fileId,webUploader,text,deleteFun){
	    	var  queue= this.parent().next('.filelist');
			if (queue.length <= 0 ) {
				queue = $('<ul class="filelist"></ul>').appendTo( $("#"+fileContent).find('.queueList') );
				$wrap = $('#'+fileContent);
				$statusBar = $wrap.find('.statusBar');
			    $statusBar.removeClass('element-invisible');
	            $statusBar.show();
				$placeHolder = $wrap.find('.placeholder');
				$placeHolder.addClass( 'element-invisible' );
	            $( '#filePicker2' ).removeClass( 'element-invisible');
	            queue.parent().addClass('filled');
	            queue.show();
	            webUploader.refresh();
	            webUploader.addButton({
	                id: '#filePicker2',
	                label: '添加资源'
	            });
			}
			if(text==""){
				  var $li = $( '<li id='+fileId+'>' +
			                '<p class="title"></p>' +
			                '<p class="imgWrap"></p>'+
			                '<p class="progress"><span></span></p>' +
			                '</li>' ),
			            $btns = $('<div class="file-panel">' +
			            		 '<span class="cancel">删除</span>' +
					                '<span class="down">下移</span>' +
					                '<span class="up">上移</span></div>').appendTo( $li ),
			            $prgress = $li.find('p.progress span'),
			            $wrap = $li.find( 'p.imgWrap' ),//文件p
			            $info = $('<p class="error"></p>');
			        //鼠标经过露出图片上的按钮
			        $li.on( 'mouseenter', function() {
			        	if (( dataSrc.split(".")[1] != 'png' ) && (dataSrc.split(".")[1] != 'jpg')) {
			        		$btns.stop().animate({height:330});
			        	}else{
			        		$btns.stop().animate({height:30});
			        	}
			        });
			        $li.on( 'mouseleave', function() {
			            $btns.stop().animate({height: 0});
			        });
			        //按钮操作事件
			        $btns.on('click','span',function() {
			            var index = $(this).index(),//span的位置
			            deg;
			            switch ( index ) {
			                case 0:
		                        var $li = $('#'+fileId);
		                        $li.off().find('.file-panel').off().end().remove();
		                        deleteFun(fileId);
			                    return;
			                case 1:
			                	var $li = $('#'+fileId);
			                	$li.insertAfter($li.next());
			                    break;
			                case 2:
			                	var $li = $('#'+fileId);
			                	$li.insertBefore($li.prev());	
			                    break;
			            }
			        });
			        $li.appendTo( queue );
			        if (( dataSrc.split(".")[1] != 'png' ) && (dataSrc.split(".")[1] != 'jpg')) {
						$wrap.parents("#"+fileId).addClass("file_diy_fileContent_video");
					}else{
						$wrap.append('<img src="'+dataSrc+'" onclick="divUploadImgOnClick(this);">');
					}
			}else{
				var $li =$("<li style='font-size: 16px;text-align:left;'><te>" + text + "</te></li>");
				var $btns = $("<div class='file-panel'><span class='cancel'>删除</span><span class='down'>下移</span><span class='up'>上移</span></div>");
			    $("#"+fileContent).find("ul").append($li);
		    	$("#"+fileContent).find("ul").find("li").last().append($btns); 
		    	$li.on('mouseenter',function() {
		       		$btns.stop().animate({height:30});
		        });
		        $li.on('mouseleave', function() {
		            $btns.stop().animate({height: 0});
		        });
		        $btns.on('click','span',function() {
		            var index = $(this).index();
		            switch(index ){
		                case 0:
		                    $(this).parents("li").remove();
		                    return;
		                case 1:
		                	$(this).parents("li").insertAfter($(this).parents("li").next());
		                    break;
		                case 2:
		                	$(this).parents("li").insertBefore($(this).parents("li").prev());	
		                    break;    
		            }
		        });
			}
		}
	});
})( jQuery );
//自定义点击图片触发事件--多选在上面逻辑中并没有加入点击触发,但配置也可以
function divUploadImgOnClick(obj){
	//针对图片在上面和下面分别作了处理
	$(obj).parents(".parentFileBox").next().find(".webuploader-element-invisible").trigger("click");
	$(obj).parents(".parentFileBox").prev().find(".webuploader-element-invisible").trigger("click");
}
