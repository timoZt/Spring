/*
 *	jQuery文件上传插件,封装UI,上传处理操作采用Baidu WebUploader;
 *  @Author 黑爪爪;
 *  @edit timo
 */
(function( $ ) {
    var  uploadFileNumberTopAce = 0;
    var  fileIds = [];
    $.fn.extend({
        /*
         *	上传方法 opt为参数配置;
         *	serverCallBack回调函数 每个文件上传至服务端后,服务端返回参数,无论成功失败都会调用 参数为服务器返回信息;
         *   success 上传成功回掉方法 successCallBack( file,response );  返回响应和文件
         *   error  上传错误回掉方法 errorCallBack( err ); 返回队伍信息
         *   multiple 是否支持多文件上传--在导入配置以后并没有删除属性
         *   buttonText 按钮文字--不传入便没有选择文件按钮
        *      startUpload 上传回调事件
         *   startUpButton 是否开启全部上传按钮
         */
        diyUpload:function( opt, serverCallBack ) {
            if ( typeof opt != "object" ) {
                layer.alert('参数错误!',{icon:5});
                return;
            }
            var $fileInput = $(this);
            var $fileInputId = $fileInput.attr('id');
            //组装参数;
            if( opt.url ) {
                opt.server = opt.url;
                delete opt.url;
            }
            if( opt.oss ) {
                var oss = opt.oss;
                delete opt.oss;
            }
            if( opt.success ) {
                var successCallBack = opt.success;
                delete opt.success;
            }
            if(opt.uploadFinished){
                var uploadFinished = opt.uploadFinished;
                delete opt.uploadFinished;
            }
            if( opt.error ) {
                var errorCallBack = opt.error;
                delete opt.error;
            }
            if(opt.startUpload){
                var startUpload=opt.startUpload;
                delete opt.startUpload;
            }

            //迭代出默认配置
            $.each( getOption( '#'+$fileInputId ),function( key, value ){
                opt[ key ] = opt[ key ] || value;
            });
            if(opt.multiple!=null){
                opt.pick.multiple=opt.multiple;
                //这里不做删除
            }
            if (opt.buttonText ) {
                opt['pick']['id'] =  $fileInput;
                opt['pick']['label'] = opt.buttonText;
                delete opt.buttonText;
            }else{
                opt['pick']['id'] =  $fileInput;
                $fileInput.hide();
            }
            var webUploader = getUploader( opt );
            if ( !WebUploader.Uploader.support() ) {
                layer.alert( ' 上传组件不支持您的浏览器！',{icon:5});
                return false;
            }
            if(startUpload){
                webUploader.on('startUpload',function(file){
                    startUpload();
                });
            }
            //绑定文件加入队列事件;
            webUploader.on('fileQueued', function( file ) {
                createBox( $fileInput, file ,webUploader,opt);
            });
            //当文件被加入队列之前触发，此事件的handler返回值为false，则此文件不会被添加进入队列。
            webUploader.on('beforeFileQueued', function( file ) {
                if(opt.multiple!=null && !opt.multiple){
                    //单选删除原本的文件
                    var fileArr = webUploader.getFiles( 'queued' );
                    $.each( fileArr ,function( i, v ){
                        removeLi( $('#fileBox_'+v.id), v.id, webUploader );
                    });
                    webUploader.reset();//重置队列
                    if(opt.imgTop){
                        $fileInput.prev('.parentFileBox').remove();
                    }else{
                        $fileInput.next('.parentFileBox').remove();
                    }
                }
            });
            //进度条事件
            webUploader.on('uploadProgress',function( file, percentage  ){
                var $fileBox = $('#fileBox_'+file.id);
                var $diyBar = $fileBox.find('.diyBar');
                $diyBar.show();
                percentage = percentage*100;
                showDiyProgress( percentage.toFixed(2), $diyBar);

                var $diyBar1 = $('#'+file.id+'_file');
                var $diyBar2 = $('#'+file.id+'_fileWidth');
                showDiyProgressOfAce(percentage.toFixed(2), $diyBar1);
                showDiyProgressOfAceWidth( percentage.toFixed(2), $diyBar2);
            });

            //全部上传结束后触发;
            webUploader.on('uploadFinished', function(){
                $fileInput.next('.parentFileBox').children('.diyButton').remove();
                if(uploadFinished){
                    uploadFinished();
                }
            });
            //绑定发送至服务端返回后触发事件;
            webUploader.on('uploadAccept', function( object ,data ){
                if ( serverCallBack ) serverCallBack( data );
            });

            //上传成功后触发事件;
            webUploader.on('uploadSuccess',function( file, response ){
                var $fileBox = $('#fileBox_'+file.id);
                var $diyBar = $fileBox.find('.diyBar');
                $fileBox.removeClass('diyUploadHover');
                $diyBar.fadeOut( 1000 ,function(){
                    $fileBox.children('.diySuccess').show();
                });
                if ( successCallBack ) {
                    successCallBack( file,response );
                }
            });

            //上传失败后触发事件;
            webUploader.on('uploadError',function( file, reason ){
                var $fileBox = $('#fileBox_'+file.id);
                var $diyBar = $fileBox.find('.diyBar');
                showDiyProgress( 0, $diyBar , '上传失败!' );

                var $diyBar1 = $('#'+file.id+'_file');
                showDiyProgressOfAce(0, $diyBar1, '上传失败!');

                var err = '上传失败! 文件:'+file.name+' 错误码:'+reason;
                if ( errorCallBack ) {
                    errorCallBack( err );
                }
            });

            //错误触发事件;
            webUploader.on('error', function( code ) {
                var text = '';
                switch( code ) {
                    case  'F_DUPLICATE' : text = '该文件已经被选择了!' ;
                        break;
                    case  'Q_EXCEED_NUM_LIMIT' : text = '上传文件数量超过限制!' ;
                        break;
                    case  'F_EXCEED_SIZE' : text = '文件大小超过限制!';
                        break;
                    case  'Q_EXCEED_SIZE_LIMIT' : text = '所有文件总大小超过限制!';
                        break;
                    case 'Q_TYPE_DENIED' : text = '文件类型不正确或者是空文件!';
                        break;
                    default : text = '未知错误!';
                        break;
                }
                layer.alert( text,{icon:5});
            });

            if(opt.chunked){
                var radom = random_string(20);
            }

            if(oss){
                var objdata = {
                    // upfile_endpoint:'http://qssd.oss-cn-shenzhen.aliyuncs.com',//上传地址
                    // upfile_nametype:'random_name',//local_name random_name  上传文件的文件名类型
                    // upfile_defaltdir:'upload/'//上传路径 多层  格式  upload/floder1/floder2
                };
                webUploader.on('uploadBeforeSend', function (obj, data, headers) {
                    //如果同一个页面上传多次  需要处理签名逻辑   不用每次都签名
                    $.ajax({
                        type : "post",
                        url : oss_postObjectPolicy,
                        timeout : 10000,
                        async: false,
                        dataType:"json",
                        data : {
                            "dir" : oss_upfile_defaltdir,
                            "callback":oss_callBack
                        },
                        success : function(str) {
                            if (str) {
                                try {
                                    var re = str;
                                    if(!stringNotNull(radom)){
                                        var key = re.dir+random_string(20)+get_suffix(data.name);
                                    }else {
                                        var key = re.dir+radom+get_suffix(data.name);
                                    }
                                    objdata.osssignature = {
                                        'key' : key,
                                        'policy': re.policy,
                                        'OSSAccessKeyId': re.accessid,
                                        'success_action_status' : '200', //让服务端返回200,不然，默认会返回204
                                        'signature': re.signature,
                                        'callback' : re.callback,
                                        'host':re.host,
                                        'x:var':key
                                    };
                                    // img.option("server",re.host);
                                } catch (e) {
                                    alert("系统错误:"+e);
                                }

                            } else {
                                alert("结果为空");
                            }
                        },
                        error : function(XMLHttpRequest, textStatus, errorThrown) {
                            alert("ajax error");
                        },
                        complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                            if(status == 'timeout'){
                                alert('请求超时，请稍后再试！');
                            }
                        }
                    });
                    //赋值参数
                    data = $.extend(data,objdata.osssignature);
                    //设置文件路径
                    /* data.key = data.key + "/" + calculate_object_name(data.name,objdata.upfile_nametype);*/
                    obj.filepath = data.key;
                    // console.log(data);
                    /*file.path = data.key;*/
                    /* _.extend(headers, {
                     /!* "Origin": "*",*!/
                     "Access-Control-Request-Method": "POST"
                     });*/
                    /* headers["Access-Control-Allow-Origin"]="*";*/
                });
            }
            return webUploader;
        }
    });

    //Web Uploader默认配置;
    function getOption(objId) {
        /*
         *	配置文件同webUploader一致,这里只给出默认配置.
         *	具体参照:http://fex.baidu.com/webuploader/doc/index.html
         */
        return {
            //按钮容器;
            pick:{
//				id:objId
//				label:"点击选择文件"
            },
            //类型限制;
            accept:{
                title:"Images",
                extensions:"gif,jpg,jpeg,bmp,png",
                mimeTypes:"image/*"
            },
            //配置生成缩略图的选项
            thumb:{
                width:170,
                height:150,
                // 图片质量，只有type为`image/jpeg`的时候才有效。
                quality:70,
                // 是否允许放大，如果想要生成小图的时候不失真，此选项应该设置为false.
                allowMagnify:false,
                // 是否允许裁剪。
                crop:true,
                // 为空的话则保留原有图片格式。
                // 否则强制转换成指定的类型。
                type:"image/jpeg"
            },
            //文件上传方式
            method:"POST",
            //服务器地址;
            server:"",
            //是否已二进制的流的方式发送文件，这样整个上传内容php://input都为文件内容
            sendAsBinary:false/*,
             // 开起分片上传。 thinkphp的上传类测试分片无效,图片丢失;
             chunked:true,
             // 分片大小
             chunkSize:512 * 1024,
             //最大上传的文件数量, 总文件大小,单个文件大小(单位字节);
             fileNumLimit:50,
             fileSizeLimit:5000 * 1024,
             fileSingleSizeLimit:500 * 1024*/
        };
    }

    //实例化Web Uploader
    function getUploader( opt ) {
        return new WebUploader.Uploader( opt );;
    }

    //操作进度条;
    function showDiyProgress( progress, $diyBar, text ) {
        if ( progress >= 100 ) {
            progress = progress + '%';
            text = text || '上传完成';
        } else {
            progress = progress + '%';
            text = text || progress;
        }
        var $diyProgress = $diyBar.find('.diyProgress');
        var $diyProgressText = $diyBar.find('.diyProgressText');
        $diyProgress.width( progress );
        $diyProgressText.text( text );
    }
    //操作进度条;
    function showDiyProgressOfAceWidth( progress, $diyBar ) {
        progress = progress + '%';
        $diyBar.width( progress );
    }
    //操作进度条;
    function showDiyProgressOfAce( progress, $diyBar,text1) {
        if ( progress == 100 ) {
            progress = '上传完成';
            //防止第二次--精度造成的问题
            if(uploadFileNumberTopAce==0){
                uploadFileNumberTopAce=1;
                $("#uploadFileNumber").text(parseInt($("#uploadFileNumber").text())-1);
                $("#uploadFileNumberTop").text(parseInt($("#uploadFileNumberTop").text())-1);
            }else if(uploadFileNumberTopAce==1){
                uploadFileNumberTopAce=0;
            }
        } else {
            progress = progress + '%';
        }
        if(text1!=null && text1!=""){
            $diyBar.text( text1 );
        }else{
            $diyBar.text( progress );
        }
    }

    //取消事件;
    function removeLi ( $li ,file_id ,webUploader) {
        webUploader.removeFile( file_id );
        if ( $li.siblings('li').length <= 0 ) {
            $li.parents('.parentFileBox').remove();
        } else {
            $li.remove();
        }

    }

    //创建文件操作div;
    function createBox( $fileInput, file, webUploader,opt) {
        if(opt.imgTop){
            $fileInput.prev('.updateUploadImg').remove();//删除修改时显示在原本位置的图片
        }else{//如果没有值的话默认图片在上方，这此行代码亦无效
            $fileInput.next('.updateUploadImg').remove();//删除修改时显示在原本位置的图片
        }
        var  queue= $fileInput.parent().next('.filelist');
        if(opt.fileContent){
            if ( queue.length <= 0 ) {
                queue = $('<ul class="filelist"></ul>').appendTo( $("#"+opt.fileContent).find('.queueList') );
            }
        }
        var file_id = file.id;

        var $parentFileBox;
        if(opt.imgTop){
            $parentFileBox = $fileInput.prev('.parentFileBox');
        }else{
            $parentFileBox = $fileInput.next('.parentFileBox');
        }
        //添加父系容器;
        if ( $parentFileBox.length <= 0 ) {
            var div = '<div class="parentFileBox"> \
                        <ul class="fileBoxUl"></ul>\
                    </div>';
            if(opt.imgTop){
                $fileInput.before(div);
                $parentFileBox = $fileInput.prev('.parentFileBox');
            }else{
                $fileInput.after( div );
                $parentFileBox = $fileInput.next('.parentFileBox');
            }
        }
        //创建按钮
        if ( $parentFileBox.find('.diyButton').length <= 0 && opt.startUpButton ) {
            var div = '<div class="diyButton"> \
                        <a class="diyStart" href="javascript:void(0)">开始上传</a> \
                        <a class="diyCancelAll" href="javascript:void(0)">全部取消</a> \
                    </div>';
            $parentFileBox.append( div );
            var $startButton = $parentFileBox.find('.diyStart');
            var $cancelButton = $parentFileBox.find('.diyCancelAll');
            //开始上传,暂停上传,重新上传事件;
            var uploadStart = function (){
                webUploader.upload();
                /*$startButton.text('暂停上传').one('click',function(){
                 webUploader.stop();
                 $(this).text('继续上传').one('click',function(){
                 uploadStart();
                 });
                 });*/
            }
            //绑定开始上传按钮;
            $startButton.one('click',uploadStart);
            //绑定取消全部按钮;
            $cancelButton.bind('click',function(){
                var fileArr = webUploader.getFiles( 'queued' );
                $.each( fileArr ,function( i, v ){
                    removeLi( $('#fileBox_'+v.id), v.id, webUploader );
                });
            });
        }

        //添加子容器;
        var li = '<li id="fileBox_'+file_id+'" class="diyUploadHover"> \
                    <div class="viewThumb"></div> \
                    <div class="diyCancel"></div> \
                    <div class="diySuccess"></div> \
                    <div class="diyFileName">'+file.name+'</div>\
                    <div class="diyBar"> \
                            <div class="diyProgress"></div> \
                            <div class="diyProgressText">0%</div> \
                    </div> \
                </li>';
        $parentFileBox.children('.fileBoxUl').append( li );
        //父容器宽度;
        var $width = $('.fileBoxUl>li').length * 200;
        var $maxWidth = $fileInput.parent().width();
        $width = $maxWidth > $width ? $width : $maxWidth;
        $parentFileBox.width( $width );

        var $fileBox = $parentFileBox.find('#fileBox_'+file_id);

        //绑定取消事件;
        var $diyCancel = $fileBox.children('.diyCancel').one('click',function(){
            removeLi( $(this).parent('li'), file_id, webUploader );
        });
        //判断文件的后缀-然后选定对应的图片作为背景图 $fileBox为文件li
        if ( file.type.split("/")[0] != 'image' ) {
            var liClassName = getFileTypeClassName( file.name.split(".").pop() );
            $fileBox.addClass(liClassName);
            $fileBox.find('.viewThumb').attr("onclick","divUploadImgOnClick(this);");
            return;
        }
        //生成预览缩略图;
        webUploader.makeThumb( file, function( error, dataSrc ) {
            if ( !error ) {
                //如果是图片
//					if(opt.accept)
                if(opt.multiple!=null && !opt.multiple){//单选
                    $fileBox.find('.viewThumb').append('<img src="'+dataSrc+'" onclick="divUploadImgOnClick(this);">');
                }else{
                    $fileBox.find('.viewThumb').append('<img src="'+dataSrc+'" >');
                }
            }
        });
    }
    //获取文件类型;
    function getFileTypeClassName ( type ) {
        var fileType = {};
        var suffix = '_diy_bg';
        fileType['pdf'] = 'pdf';
        fileType['zip'] = 'zip';
        fileType['rar'] = 'rar';
        fileType['csv'] = 'csv';
        fileType['doc'] = 'doc';
        fileType['xls'] = 'xls';
        fileType['xlsx'] = 'xls';
        fileType['txt'] = 'txt';

        fileType['flv'] = 'flv';
        fileType['mp4'] = 'mp4';
        fileType['apk'] = 'apk';

        fileType = fileType[type] || 'file';
        return 	fileType+suffix;
    }

    $.fn.extend({
        //修改时原本的图片--普通上传-
        //dataSrc图片路径-
        //deleteFun图片删除回掉方法,返回了图片路径-
        //imgTop图片位置，值为true上false下
        updateUploadImg:function(dataSrc,imgTop,deleteFun){
            //updateUploadImg-标注是修改的图片div-在创建新的div的时候会做删除
            var div = '<div class="parentFileBox  updateUploadImg"> \
								<ul class="fileBoxUl"></ul>\
							</div>';
            if(imgTop){
                $(this).before(div);
                $parentFileBox = $(this).prev('.parentFileBox');
            }else{
                $(this).after(div);
                $parentFileBox = $(this).next('.parentFileBox');
            }
            //添加子容器;
            var li = '<li id="fileBox_" class="diyUploadHover"> \
							<div class="viewThumb" id="viewThumb"></div> \
							<div class="diyCancel" ></div> \
							<div class="diySuccess"></div> \
							<div class="diyBar"> \
									<div class="diyProgress"></div> \
									<div class="diyProgressText">0%</div> \
							</div> \
						</li>';
            $parentFileBox.children('.fileBoxUl').append( li );
            var $fileBox = $parentFileBox.find('#fileBox_');
            $fileBox.find('.viewThumb').append('<img src="'+dataSrc+'" onclick="divUploadImgOnClick(this);">');
            if(deleteFun){
                $fileBox.find(".diyCancel").bind("click",function(){
                    var  dataUrl = $(this).prev().find("img").attr("src");
                    deleteFun(dataUrl);
                    $(this).parents(".diyUploadHover").remove();
                });
            }else{
                $fileBox.find(".diyCancel").remove();
            }
        }
    });

    $.fn.extend({
        //修改时原本的图片集合
        //dataSrc是一个路径数组,
        //deleteFun图片删除回掉方法,返回了图片路径-
        //imgTop图片位置，值为true上false下
        updateUploadImgs:function(dataSrc,imgTop,deleteFun){
            var div = '<div class="parentFileBox "> \
								<ul class="fileBoxUl"></ul>\
							</div>';
            if(imgTop){
                $(this).before(div);
                $parentFileBox = $(this).prev('.parentFileBox');
            }else{
                $(this).after(div);
                $parentFileBox = $(this).next('.parentFileBox');
            }
            for(var i =0 ; i<dataSrc.length;i++){
                //添加子容器;
                var li = '<li id="fileBox_'+i+'" class="diyUploadHover"> \
								<div class="viewThumb"></div> \
								<div class="diyCancel" ></div> \
								<div class="diySuccess"></div> \
								<div class="diyBar"> \
										<div class="diyProgress"></div> \
										<div class="diyProgressText">0%</div> \
								</div> \
							</li>';
                $parentFileBox.children('.fileBoxUl').append( li );
                var $fileBox = $parentFileBox.find('#fileBox_'+i);
                $fileBox.find('.viewThumb').append('<img src="'+dataSrc[i]+'"  >');
                if(deleteFun){
                    $fileBox.find(".diyCancel").bind("click",function(){
                        var  dataUrl = $(this).prev().find("img").attr("src");
                        deleteFun(dataUrl);
                        $(this).parents(".diyUploadHover").remove();
                    });
                }else{
                    $fileBox.find(".diyCancel").remove();
                }
            }
        }
    });
})( jQuery );
//自定义点击图片触发事件--多选在上面逻辑中并没有加入点击触发,但配置也可以
function divUploadImgOnClick(obj){
    //针对图片在上面和下面分别作了处理
    $(obj).parents(".parentFileBox").next().find(".webuploader-element-invisible").trigger("click");
    $(obj).parents(".parentFileBox").prev().find(".webuploader-element-invisible").trigger("click");
}
function random_string(len) {
    len = len || 32;
    var chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
    var maxPos = chars.length;
    var pwd = '';
    for (i = 0; i < len; i++) {
        pwd += chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
}

function get_suffix(filename) {
    var pos = filename.lastIndexOf('.')
    var suffix = ''
    if (pos != -1) {
        suffix = filename.substring(pos)
    }
    return suffix;
}
