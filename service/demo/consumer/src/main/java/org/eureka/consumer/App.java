package org.eureka.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient//标识为服务消费者
@EnableFeignClients//使用feign
@EnableHystrix//断路器
@EnableHystrixDashboard
@ComponentScan("org.eureka.consumer.**")
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
    /**
	 * @LoadBalanced深坑，在order-service中使用的是http://user-service/user 来访问user-service服务中的接口，
	 * 由于没有在RestTemplate中加@LoadBalanced注解导致一直找不到user-service服务，
	 * 这个是Ribbon中的轮询策略，@LoadBalanced表示负载均衡。
	 * <p>Title: restTemplate</p>  
	 * <p>Description: </p>  
	 * @return
	 */
	@Bean
    @LoadBalanced 
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
