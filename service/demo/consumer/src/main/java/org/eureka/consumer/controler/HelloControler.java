package org.eureka.consumer.controler;

import org.eureka.consumer.feign.SchedualServiceHi;
import org.eureka.consumer.ribbon.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/** 
* @author timo E-mail: 13551883869@sina.cn
* @version 创建时间：2018年4月9日 下午3:28:16 
* 类说明 
*/
@RestController
@RefreshScope//动态mq通知刷新必备 注解到获取配制文件的类上
public class HelloControler {

    @Autowired
    HelloService helloService;
    @Autowired
    SchedualServiceHi schedualServiceHi;
    

    @Value("${foo}")
    String foo;
    
    @RequestMapping(value = "/hi3")
    public String hi3(){
        return foo;
    }
    
    //rest+ribbon
    @RequestMapping(value = "/hi")
    public String hi(@RequestParam String name){
        return helloService.hiService(name);
    }
    //feign
    @RequestMapping(value = "/hi2")
    public String hi2(@RequestParam String name){
        return schedualServiceHi.sayHiFromClientOne(name);
    }


}
