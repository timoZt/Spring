package org.eureka.consumer.feign;

import org.springframework.stereotype.Component;

/** 
* @author timo E-mail: 
* @version 创建时间：2018年5月3日 上午10:54:09 
* 类说明 
*/
@Component
public class SchedualServiceHiHystric implements SchedualServiceHi {
    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry "+name;
    }
}
