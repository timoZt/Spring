package com.cloudJeesite.framework.frameworkCore.common.uitls.returnUtil;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * 分页类
 * @author timo
 * @version 1.0
 */
public class PageUtils {
	/**
	 * 一个通用的创建分页请求的方法
	 * @param pageNumber	页码
	 * @param pageSize		一页内数据条数
	 * @param sortField		排序字段
	 * @param orderType		 排序字段的排序类型[为空的话，则默认是倒序排列]
	 * @return
	 */
	public static PageRequest buildPageRequest(int pageNumber, int pageSize, @RequestParam(value="sortField",required=false,defaultValue = "createDate") String sortField, @RequestParam(value="orderType",required=false,defaultValue = "desc") String orderType) {
		Sort sort = null;
		Direction sortTypeEnum = Direction.DESC;
		if (StringUtils.isNotBlank(orderType) && ReTurnAppConfig.SORT_TYPE_ASC.equals(orderType.toLowerCase())) {
			sortTypeEnum = Direction.ASC;
		}
		if (StringUtils.isNotBlank(sortField)) {
			sort = new Sort(sortTypeEnum, sortField);
			
		}
		pageNumber = (pageNumber <= 0) ? 1 : pageNumber;
		pageSize = (pageSize <= 0) ? ReTurnAppConfig.PAGE_SIZE_DEFAULT : pageSize;
		return sort == null ?new PageRequest(pageNumber - 1, pageSize) : new PageRequest(pageNumber - 1, pageSize, sort);
	}
	/**
	 * 一个通用的创建分页请求的方法
	 * @param pageNumber	页码
	 * @param pageSize		一页内数据条数
	 * @param sortField		排序字段
	 * @param sortField1		排序字段
	 * @param orderType		 排序字段的排序类型[为空的话，则默认是倒序排列]
	 * @return
	 */
	public static PageRequest buildPageRequests(int pageNumber, int pageSize, @RequestParam(value="sortField",required=false) String sortField, @RequestParam(value="sortField1",required=true) String sortField1, @RequestParam(value="orderType",required=true) String orderType) {
		Sort sort = null;
		Direction sortTypeEnum = Direction.DESC;
		if (StringUtils.isNotBlank(orderType) && ReTurnAppConfig.SORT_TYPE_ASC.equals(orderType.toLowerCase())) {
			sortTypeEnum = Direction.ASC;
		}
//		if (StringUtils.isNotBlank(sortField)) {
//			sort = new Sort(sortTypeEnum, sortField, sortField1);
//
//		}
        List<String> sorts = new ArrayList<String>();
        if (StringUtils.isNotBlank(sortField1)) {
            sorts.add(sortField1);
        }
        if (StringUtils.isNotBlank(sortField)) {
            sorts.add(sortField);
        }
        if (sorts.isEmpty()) {
            sorts.add("createDate");
        }
        sort = new Sort(sortTypeEnum, sorts);
        pageNumber = (pageNumber <= 0) ? 1 : pageNumber;
		pageSize = (pageSize <= 0) ? ReTurnAppConfig.PAGE_SIZE_DEFAULT : pageSize;
		return sort == null ?new PageRequest(pageNumber - 1, pageSize) : new PageRequest(pageNumber - 1, pageSize, sort);
	}


}
