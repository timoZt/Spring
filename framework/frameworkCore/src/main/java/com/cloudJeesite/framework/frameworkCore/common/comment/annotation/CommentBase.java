package com.cloudJeesite.framework.frameworkCore.common.comment.annotation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 添加注释初始类
 * @author psc
 *
 */
@Component
public class CommentBase {
	//数据库类型 mysql  oracle  sqlserver
	@Value("${init.databaseName}")
	private String databaseType;
	//项目实体类包路径
	@Value("${init.domainPath}")
	private String[] packagePath;
	//mysql的数据库名称
	@Value("${init.mysqlDatabaseName}")
	private String mysqlTableSchema;
	
	public String getDatabaseType() {
		return databaseType;
	}
	public void setDatabaseType(String databaseType) {
		this.databaseType = databaseType.toLowerCase();
	}
	public String[] getPackagePath() {
		return packagePath;
	}
	public void setPackagePath(String... packagePath) {
		this.packagePath = packagePath;
	}
	public String getMysqlTableSchema() {
		return mysqlTableSchema;
	}
	public void setMysqlTableSchema(String mysqlTableSchema) {
		this.mysqlTableSchema = mysqlTableSchema;
	}
}
