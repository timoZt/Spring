package com.cloudJeesite.framework.frameworkCore.common.uitls.enumUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import com.cloudJeesite.framework.frameworkCore.common.mapper.JsonMapper;
import com.cloudJeesite.framework.frameworkCore.common.uitls.reflection.Reflections;
import com.google.common.collect.Lists;

/**
 * Created by timo on 2017/12/12.
 */
public class EnumUtils {
    //支付方式
    public static final String paymentWayPath="com.springBoot.springBootSysCore.modules.enums.PaymentWay";
    //审核状态
    public static final String auditStateEnumPath="com.springBoot.springBootSysCore.modules.enums.AuditStateEnum";
    //用户类型
    public static final String userTypeEnumPath ="com.springBoot.springBootSysCore.modules.enums.base.UserTypeEnum";

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<EnumKeyVal> beankeyValList(String enumUitlClassName) {
        try {
            Class cas = Class.forName(Reflections.getFieldValue(new EnumUtils(),enumUitlClassName).toString());
            Method m = cas.getMethod("keyValList");
            List<EnumKeyVal> li = (List<EnumKeyVal>) m.invoke(cas);
            if (li == null){
                li = Lists.newArrayList();
            }
            return li;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static String keyValList(String enumUitlClassName) {
        try {
            Class cas = Class.forName(Reflections.getFieldValue(new EnumUtils(),enumUitlClassName).toString());
            Method m = cas.getMethod("keyValList");
            List<EnumKeyVal> li = (List<EnumKeyVal>) m.invoke(cas);
            if (li == null){
                li = Lists.newArrayList();
            }
            return JsonMapper.toJsonString(li);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static String getNameByVal(String enumUitlClassName,String val) {
        try {
            Class cas = Class.forName(Reflections.getFieldValue(new EnumUtils(),enumUitlClassName).toString());
            Method m = cas.getMethod("nameFind",String.class,String.class);
            return m.invoke(cas,val,"未知").toString();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static String getValByName(String enumUitlClassName,String name) {
        try {
            Class cas = Class.forName(Reflections.getFieldValue(new EnumUtils(),enumUitlClassName).toString());
            Method m = cas.getMethod("valfind",String.class,String.class);
            return m.invoke(cas,name,"未知").toString();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
//    public static List<EnumKeyVal> keyValList(String enumClassName) {
//        try {
//            Class cas = Class.forName(enumClassName);
//            Method m = cas.getMethod("keyValList");
//            return (List<EnumKeyVal>) m.invoke(cas);
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
}
