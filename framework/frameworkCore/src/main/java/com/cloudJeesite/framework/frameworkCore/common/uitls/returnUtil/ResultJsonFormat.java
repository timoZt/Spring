package com.cloudJeesite.framework.frameworkCore.common.uitls.returnUtil;


import com.google.gson.annotations.Expose;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:  返回给客户端的参数信息
 * @ClassName: ResultJsonFormat
 * @Author: timo
 */
public class ResultJsonFormat {

	/** 请求返回的Json中的信息MSG */
	@Expose
	private String msg="成功";
	/** 请求返回数据对象(json || obj) */
	@Expose
	private Object data;
	/** 请求返回数据对象(json || obj) */
	private Object rows;
	/** 是否存在下一页 */
	private boolean next = false;
	/** 页码 */
	private Long pageNumber;
	/** 数据的总条数 */
	private Long totalElements;
	/**总页数**/
	private Long pageTotal;
	/** 是否成功 默认1为成功 */
	@Expose
	private Integer flag = 200;
	
	private String isUpdate ="true";

	public ResultJsonFormat(Integer flag) {
		super();
		this.flag = flag;
	}
	/**
	 * 适用于添加、修改、删除
	 * 
	 * @param msg  消息
	 * @param flag 是否成功 true成功 false失败
	 */
	public ResultJsonFormat(String msg, Integer flag) {
		super();
		this.msg = msg;
		this.flag = flag;
	}

	/**
	 * 只返回数据和标记成功和失败
	 * 
	 * @param data
	 * @param flag
	 */
	public ResultJsonFormat(Object data, Integer flag) {
		super();
		this.data = data;
		this.rows = data; //用于easyui数据封装
		this.flag = flag;
	}

	/**
	 * 适用于添加、修改、删除
	 * 
	 * @param msg 	消息
	 * @param data 	操作后的返回数据
	 * @param flag  是否成功
	 */
	public ResultJsonFormat(String msg, Object data, Integer flag) {
		super();
		this.msg = msg;
		this.rows = data; //用于easyui数据封装
		this.data = data;
		this.flag = flag;
	}

	/**
	 * 集合数据
	 * 
	 * @param msg	消息
	 * @param data	数据
	 * @param next	是否有下一页
	 * @param page	页码
	 * @param total	总条数
	 * @param pageSize 每页条数
	 * @param flag	是否成功
	 */
	public ResultJsonFormat(String msg, Object data, boolean next, Long pageNumber,Long pageSize,
			Long totalElements, Integer flag) {
		super();
		this.msg = msg;
		this.data = data;
		this.rows=data; //用于easyui数据封装
		this.next = next;
		this.pageNumber = pageNumber;
		this.totalElements = totalElements;
		this.pageTotal= totalElements%pageSize ==0 ? totalElements/pageSize : totalElements/pageSize+1;
		this.flag = flag;
	}
	public ResultJsonFormat(String msg,Object rows, boolean next, Integer pageNumber,Integer pageSize,
			Long totalElements, Integer flag) {
		super();
		this.msg = msg;
		this.data = rows;
		this.rows=rows; //用于easyui数据封装
		this.next = next;
		this.pageNumber = pageNumber.longValue();
		this.totalElements = totalElements;
		this.pageTotal=pageSize==0?0:totalElements%pageSize ==0 ? totalElements/pageSize : totalElements/pageSize+1;
		this.flag = flag;
	}
	/**
	 * 集合数据 easyui
	 * 
	 * @param msg	消息
	 * @param rows	数据
	 * @param next	是否有下一页
	 * @param page	页码
	 * @param total	总条数
	 * @param flag	是否成功
	 */
	public ResultJsonFormat(String msg,Object rows, boolean next, Long pageNumber,
			Long totalElements, Integer flag) {
		super();
		this.msg = msg;
		this.rows = rows;
		this.next = next;
		this.pageNumber = pageNumber;
		this.totalElements = totalElements;
		this.flag = flag;
	}

	/**
	 * 自动处理需要传递到客户端或则网页的数据(包含rows和data)
	 * @return
	 */
	public Map<String, Object> convertResultJson() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if (msg != null && !"".equals(msg)) {
			resultMap.put(ReTurnAppConfig.KEY_MSG, msg);
		}
		if (data != null) {
			resultMap.put(ReTurnAppConfig.KEY_DATA, data);
		}
		if (rows != null) {
			resultMap.put(ReTurnAppConfig.KEY_ROWS, rows);
		}
		if (next) {
			resultMap.put(ReTurnAppConfig.KEY_NEXT, next);
		}
		if (pageNumber != null) {
			pageNumber = pageNumber>0?pageNumber:1;
			resultMap.put(ReTurnAppConfig.KEY_PAGE, pageNumber);
		}
		if (totalElements != null) {
			resultMap.put(ReTurnAppConfig.KEY_TOTAL, totalElements);
		}
		if(pageTotal !=null){
			pageTotal = pageTotal>0?pageTotal:1;
			resultMap.put(ReTurnAppConfig.KEY_TOTAL_PGE, pageTotal);
		}
		resultMap.put(ReTurnAppConfig.KEY_FLAG, flag);
		resultMap.put("isUpdate", isUpdate);
		return resultMap;
	}
	/**
	 * 自动处理需要传递到客户端或则网页的数据(只包含data)
	 * @return
	 */
	public Map<String, Object> convertResultJsonForData() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if (msg != null && !"".equals(msg)) {
			resultMap.put(ReTurnAppConfig.KEY_MSG, msg);
		}
		if (data != null) {
			resultMap.put(ReTurnAppConfig.KEY_DATA, data);
		}
		if (next) {
			resultMap.put(ReTurnAppConfig.KEY_NEXT, next);
		}
		if (pageNumber != null) {
			pageNumber = pageNumber>0?pageNumber:1;
			resultMap.put(ReTurnAppConfig.KEY_PAGE, pageNumber);
		}
		if (totalElements != null) {
			resultMap.put(ReTurnAppConfig.KEY_TOTAL, totalElements);
		}
		if(pageTotal !=null){
			pageTotal = pageTotal>0?pageTotal:1;
			resultMap.put(ReTurnAppConfig.KEY_TOTAL_PGE, pageTotal);
		}
		resultMap.put(ReTurnAppConfig.KEY_FLAG, flag);
		resultMap.put("isUpdate", isUpdate);
		return resultMap;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getRows() {
		return rows;
	}

	public void setRows(Object rows) {
		this.rows = rows;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public boolean isNext() {
		return next;
	}

	public void setNext(boolean next) {
		this.next = next;
	}

	public Long getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Long pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(Long totalElements) {
		this.totalElements = totalElements;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Long getPageTotal() {
		return pageTotal;
	}

	public void setPageTotal(Long pageTotal) {
		this.pageTotal = pageTotal;
	}
	public String getIsUpdate() {
		return isUpdate;
	}
	public void setIsUpdate(String isUpdate) {
		this.isUpdate = isUpdate;
	}
	
	public void isAdd(){
		this.isUpdate = "false";
	}
	
	
	public void setResultInfo(Integer flag,String msg,Object data) {
		this.msg = msg;
		this.flag=flag;
		this.data=data;
	}
	public void setResultInfo(Integer flag,String msg) {
		this.msg = msg;
		this.flag=flag;
	}
	
	
	
}
