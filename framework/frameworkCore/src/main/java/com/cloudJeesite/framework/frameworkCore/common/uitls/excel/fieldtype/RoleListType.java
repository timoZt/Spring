///**
// * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
// */
//package com.cloudJeesite.framework.frameworkCore.common.uitls.excel.fieldtype;
//
//import java.util.List;
//
//import com.cloudJeesite.framework.frameworkCore.common.uitls.Collections3;
//import com.cloudJeesite.framework.frameworkCore.common.uitls.SpringContextHolder;
//import com.cloudJeesite.framework.frameworkCore.common.uitls.StringUtils;
//import com.google.common.collect.Lists;
//import com.springBoot.springBootSysCore.modules.entity.system.SysRole;
//import com.springBoot.springBootSysCore.modules.services.SystemService;
//
///**
// * 字段类型转换
// * @author ThinkGem
// * @version 2013-5-29
// */
//public class RoleListType {
//
//	private static SystemService systemService = SpringContextHolder.getBean(SystemService.class);
//	
//	/**
//	 * 获取对象值（导入）
//	 */
//	public static Object getValue(String val) {
//		List<SysRole> roleList = Lists.newArrayList();
//		List<SysRole> allRoleList = systemService.findAllRole();
//		for (String s : StringUtils.split(val, ",")){
//			for (SysRole e : allRoleList){
//				if (StringUtils.trimToEmpty(s).equals(e.getName())){
//					roleList.add(e);
//				}
//			}
//		}
//		return roleList.size()>0?roleList:null;
//	}
//
//	/**
//	 * 设置对象值（导出）
//	 */
//	public static String setValue(Object val) {
//		if (val != null){
//			@SuppressWarnings("unchecked")
//			List<SysRole> roleList = (List<SysRole>)val;
//			return Collections3.extractToString(roleList, "name", ", ");
//		}
//		return "";
//	}
//	
//}
