package com.cloudJeesite.framework.frameworkCore.common.persistence;

import org.springframework.data.annotation.Transient;

import javax.persistence.MappedSuperclass;

/**
 * ${DESCRIPTION}
 * Created by hanqunfeng on 2016/12/20 15:02.
 */

@MappedSuperclass
public class PageEntity<T> {
    @Transient
    private Integer page = 1;
    @Transient
    private Integer rows = 10;
    @Transient
    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
    @Transient
    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }
}
