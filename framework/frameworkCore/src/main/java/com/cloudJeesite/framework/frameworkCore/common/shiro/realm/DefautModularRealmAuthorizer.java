package com.cloudJeesite.framework.frameworkCore.common.shiro.realm;

import java.util.Collection;

import org.apache.shiro.authz.ModularRealmAuthorizer;
import org.apache.shiro.realm.Realm;

/**
 * Created by timo on 2017/11/28.
 */
public class DefautModularRealmAuthorizer extends ModularRealmAuthorizer {

    @Override
    public Collection<Realm> getRealms() {
        return super.getRealms();
    }
}
