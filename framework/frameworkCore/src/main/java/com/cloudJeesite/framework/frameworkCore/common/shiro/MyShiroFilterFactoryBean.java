package com.cloudJeesite.framework.frameworkCore.common.shiro;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.mgt.FilterChainResolver;
import org.apache.shiro.web.mgt.WebSecurityManager;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.apache.shiro.web.servlet.ShiroHttpServletRequest;
import org.apache.shiro.web.servlet.ShiroHttpServletResponse;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;

import com.cloudJeesite.framework.frameworkCore.common.shiro.filterChain.CustomPathMatchingFilterChainResolver;

/**
 * 自定义ShiroFilterFactoryBean 注入chainResolver
 * @author Administrator
 */
public class MyShiroFilterFactoryBean extends ShiroFilterFactoryBean {
	@Autowired
	private CustomPathMatchingFilterChainResolver chainResolver;
	
	@SuppressWarnings("rawtypes")
	@Override
	public Class getObjectType() {
		return MySpringShiroFilter.class;
	}
	public MyShiroFilterFactoryBean() {
		super();
	}
	@Override
	protected AbstractShiroFilter createInstance() throws Exception {
        SecurityManager securityManager = getSecurityManager();
        if (securityManager == null) {
            String msg = "SecurityManager property must be set.";
            throw new BeanInitializationException(msg);
        }
        if (!(securityManager instanceof WebSecurityManager)) {
            String msg = "The security manager does not implement the WebSecurityManager interface.";
            throw new BeanInitializationException(msg);
        }
        return new MySpringShiroFilter((WebSecurityManager) securityManager, chainResolver);
	}
	
	private static final class MySpringShiroFilter extends AbstractShiroFilter {
        protected MySpringShiroFilter(WebSecurityManager webSecurityManager, FilterChainResolver resolver) {
            super();
            if (webSecurityManager == null) {
                throw new IllegalArgumentException("WebSecurityManager property cannot be null.");
            }
            setSecurityManager(webSecurityManager);
            if (resolver != null) {
                setFilterChainResolver(resolver);
            }
        }
        @Override
        protected void executeChain(ServletRequest request, ServletResponse response, FilterChain origChain)
                throws IOException, ServletException {
            FilterChain chain = getExecutionChain(request, response, origChain);
//            System.out.println("执行拦截器："+chain.getClass().getName());
            chain.doFilter(request, response);
        }
        
        @Override
        protected ServletResponse wrapServletResponse(HttpServletResponse orig, ShiroHttpServletRequest request) {
        	return new MyShiroHttpServletResponse(orig,getServletContext(), request);
        }
    }
    //这个类用来去掉url后面的JSESSIONID
    static class MyShiroHttpServletResponse extends ShiroHttpServletResponse {
        public MyShiroHttpServletResponse(HttpServletResponse wrapped, ServletContext context, ShiroHttpServletRequest request) {
            super(wrapped, context, request);
        }
        @Override
        protected String toEncoded(String url, String sessionId) {
            if ((url == null) || (sessionId == null))
                return (url);
            String path = url;
            String query = "";
            String anchor = "";
            int question = url.indexOf('?');
            if (question >= 0) {
                path = url.substring(0, question);
                query = url.substring(question);
            }
            int pound = path.indexOf('#');
            if (pound >= 0) {
                anchor = path.substring(pound);
                path = path.substring(0, pound);
            }
            StringBuilder sb = new StringBuilder(path);
            sb.append(anchor);
            sb.append(query);
            return (sb.toString());
        }
    }
}


