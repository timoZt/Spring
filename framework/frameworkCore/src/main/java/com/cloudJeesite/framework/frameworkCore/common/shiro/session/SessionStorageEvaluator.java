package com.cloudJeesite.framework.frameworkCore.common.shiro.session;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.RequestPairSource;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.stereotype.Component;

import com.cloudJeesite.framework.frameworkCore.common.config.Global;

/**
 * Created by timo on 2017/12/13.
 */
@Component
public class SessionStorageEvaluator implements org.apache.shiro.mgt.SessionStorageEvaluator{
    @Override
    public boolean isSessionStorageEnabled(Subject subject) {
        boolean enabled = false;
        if(WebUtils.isWeb(subject)) {
            RequestPairSource s = (RequestPairSource) subject;
            HttpServletRequest httpReq = (HttpServletRequest) s.getServletRequest();
            String ctxPath = httpReq.getContextPath();
            String requestUri = httpReq.getRequestURI(); // 请求的全路径,比如:
            String uri = requestUri.substring(ctxPath.length());// 全路径除去ctxPath
            String tarUri = uri.trim();
            if(!tarUri.contains(Global.getConfig("apiPath"))){
                @SuppressWarnings("unused")
				HttpServletRequest request = WebUtils.getHttpRequest(subject);
                //set 'enabled' based on the current request.
            }
        } else {
            //not a web request - maybe a RMI or daemon invocation?
            //set 'enabled' another way …
        }
        return enabled;
    }
}
