package com.cloudJeesite.framework.frameworkCore.common.uitls.enumUtils;

/**
 * Created by timo on 2017/12/12.
 */
public  interface EnumKeyGetter<T extends Enum<T>, K> {

    K getKey(T enumValue);

}