package com.cloudJeesite.framework.frameworkCore.common.shiro.stateles;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.SubjectContext;
import org.apache.shiro.web.mgt.DefaultWebSubjectFactory;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-2-26
 * <p>Version: 1.0
 */
//@Component
public class StatelessDefaultSubjectFactory extends DefaultWebSubjectFactory {
    @Override
    public Subject createSubject(SubjectContext context) {
        //不创建session -- 此处由于要保持有状态的会话 所以 使用nosessioncreation拦截器配置
//        context.setSessionCreationEnabled(false);
        return super.createSubject(context);
    }
}
