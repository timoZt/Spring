package com.cloudJeesite.framework.frameworkCore.common.comment.annotation;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 数据库是Oracle时添加注解
 * @author psc
 *
 */
public class OracleDataBase {
	
	/**注释*/
	private List<String> comments = new ArrayList<String>();
	
	public OracleDataBase(JdbcTemplate jdbcTemplate,CommentBase commentBase) {
		RedClasses dd = new RedClasses();
		String[] paths = commentBase.getPackagePath();
		for (String entityPath : paths) {
			Set<Class<?>> ss = dd.getClasses(entityPath);
			MyComment meta = null;
			String tableName = null;
			String columnName = null;
			for (Class<?> table : ss) {
				Table mm = table.getAnnotation(Table.class);
				if(mm!=null){
					tableName = mm.name();
					if(tableName!=null){
						int num = jdbcTemplate.queryForObject("select count(*) from user_tables where table_name = upper('"+tableName+"')", Integer.class);
						if(num==0){
							System.out.println("表:"+tableName+" 不存在!");
							continue;
						}
					}
					meta = table.getAnnotation(MyComment.class);
					if(meta!=null){
						comments.add("COMMENT ON TABLE "+tableName+" IS '"+ meta.value()+"' ");
					}
					for (Method me : table.getMethods()) {
						meta = me.getAnnotation(MyComment.class);
						Column column = me.getAnnotation(Column.class);
						ManyToOne mto = me.getAnnotation(ManyToOne.class);
						OneToOne oneToOne = me.getAnnotation(OneToOne.class);
						JoinColumn joinColumn = me.getAnnotation(JoinColumn.class);
						if(meta!=null&&(column!=null||joinColumn!=null&&(mto!=null||oneToOne!=null))){
							columnName = column == null ? joinColumn.name() : column.name();
							int num = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM USER_TAB_COLUMNS WHERE TABLE_NAME = upper('"+tableName+"') and column_name = upper('"+columnName+"')", Integer.class);
							if(num==0){
								System.out.println("表:"+tableName+"的"+columnName+" 列不存在!");
								continue;
							}
							comments.add("COMMENT ON COLUMN "+tableName+"."+columnName+" IS '"+meta.value()+"' " );
						}
						JoinTable joinTable = me.getAnnotation(JoinTable.class);
						ManyToManyComment mtmcomment = me.getAnnotation(ManyToManyComment.class);
						if(joinTable!=null&&mtmcomment!=null){
							String mtmTable = joinTable.name();
							String joinColumnName = joinTable.joinColumns()[0].name();
							String inverseJoinColumnName = joinTable.inverseJoinColumns()[0].name();
							if(mtmTable!=null){
								int num = jdbcTemplate.queryForObject("select count(*) from user_tables where table_name = upper('"+mtmTable+"')", Integer.class);
								if(num==0){
									System.out.println("表:"+mtmTable+" 不存在!");
									continue;
								}
								comments.add("COMMENT ON TABLE "+mtmTable+" IS '"+ mtmcomment.tableComment()+"' ");
							}
							if(joinColumnName!=null){
								int num = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM USER_TAB_COLUMNS WHERE TABLE_NAME = upper('"+mtmTable+"') and column_name = upper('"+joinColumnName+"')", Integer.class);
								if(num==0){
									System.out.println("表:"+mtmTable+"的"+joinColumnName+" 列不存在!");
									continue;
								}
								comments.add("COMMENT ON COLUMN "+mtmTable+"."+joinColumnName+" IS '"+mtmcomment.joinColumn()+"' " );
							}
							if(inverseJoinColumnName!=null){
								int num = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM USER_TAB_COLUMNS WHERE TABLE_NAME = upper('"+mtmTable+"') and column_name = upper('"+inverseJoinColumnName+"')", Integer.class);
								if(num==0){
									System.out.println("表:"+mtmTable+"的"+inverseJoinColumnName+" 列不存在!");
									continue;
								}
								comments.add("COMMENT ON COLUMN "+mtmTable+"."+inverseJoinColumnName+" IS '"+mtmcomment.inverseJoinColumn()+"' " );
							}
						}
					}
				}
			}
		}
	}

	public List<String> getComments() {
		return comments;
	}
}
