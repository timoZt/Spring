package com.cloudJeesite.framework.frameworkCore.common.comment.annotation;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Mysql注释
 * @author psc
 *
 */
public class MysqlDataBase {
	/**注释*/
	private List<String> comments = new ArrayList<String>();
	
	public MysqlDataBase(JdbcTemplate jdbcTemplate,CommentBase commentBase) {
		String table_schema = commentBase.getMysqlTableSchema();
		RedClasses dd = new RedClasses();
		String[] paths = commentBase.getPackagePath();
		for (String entityPath : paths) {
			Set<Class<?>> ss = dd.getClasses(entityPath);
			MyComment meta = null;
			String tableName = null;
			String columnName = null;
			Map<String, Object> typeMap=new HashMap<String, Object>();
			for (Class<?> table : ss) {
				Table mm = table.getAnnotation(Table.class);
				if(mm!=null){
					tableName = mm.name();
					if(tableName!=null){
						int num = jdbcTemplate.queryForObject("select count(*) from information_schema.tables where table_name = '"+tableName+"' and table_schema ='"+table_schema+"'", Integer.class);
						if(num==0){
							System.out.println("表:"+tableName+" 不存在!");
							continue;
						}
					}
					meta = table.getAnnotation(MyComment.class);
					if(meta!=null){
						comments.add("ALTER TABLE "+tableName+" COMMENT '"+ meta.value()+"' ");
					}
					for (Method me : table.getMethods()) {
						meta = me.getAnnotation(MyComment.class);
						Column column = me.getAnnotation(Column.class);
						ManyToOne mto = me.getAnnotation(ManyToOne.class);
						OneToOne oneToOne = me.getAnnotation(OneToOne.class);
						JoinColumn joinColumn = me.getAnnotation(JoinColumn.class);
						if(meta!=null&&(column!=null||joinColumn!=null&&(mto!=null||oneToOne!=null))){
							columnName = column == null ? joinColumn.name() : column.name();
							int num = jdbcTemplate.queryForObject("select count(*) from information_schema.columns where table_name = '"+tableName+"' and column_name = '"+columnName+"' and table_schema ='"+table_schema+"'", Integer.class);
							if(num==0){
								System.out.println("表:"+tableName+"的"+columnName+" 列不存在!");
								continue;
							}
							typeMap = jdbcTemplate.queryForMap("select column_type as type from Information_schema.columns  where table_Name = '"+tableName+"' and column_name='"+columnName+"' and table_schema = '"+table_schema+"'");
							if(typeMap.containsKey("TYPE")){
								comments.add("ALTER TABLE "+tableName+" MODIFY COLUMN "+columnName+" "+typeMap.get("TYPE")+" COMMENT '"+ meta.value()+"' ");
							}
						}
						JoinTable joinTable = me.getAnnotation(JoinTable.class);
						ManyToManyComment mtmcomment = me.getAnnotation(ManyToManyComment.class);
						if(joinTable!=null&&mtmcomment!=null){
							String mtmTable = joinTable.name();
							String joinColumnName = joinTable.joinColumns()[0].name();
							String inverseJoinColumnName = joinTable.inverseJoinColumns()[0].name();
							if(mtmTable!=null){
								int num = jdbcTemplate.queryForObject("select count(*) from information_schema.tables where table_name = '"+mtmTable+"' and table_schema ='"+table_schema+"'", Integer.class);
								if(num==0){
									System.out.println("表:"+mtmTable+" 不存在!");
									continue;
								}
								comments.add("ALTER TABLE "+mtmTable+" COMMENT '"+ mtmcomment.tableComment()+"' ");
							}
							if(joinColumnName!=null){
								int num = jdbcTemplate.queryForObject("select count(*) from information_schema.columns where table_name = '"+mtmTable+"' and column_name = '"+joinColumnName+"' and table_schema ='"+table_schema+"'", Integer.class);
								if(num==0){
									System.out.println("表:"+mtmTable+"的"+joinColumnName+" 列不存在!");
									continue;
								}
							}
							if(inverseJoinColumnName!=null){
								int num = jdbcTemplate.queryForObject("select count(*) from information_schema.columns where table_name = '"+mtmTable+"' and column_name = '"+inverseJoinColumnName+"' and table_schema ='"+table_schema+"'", Integer.class);
								if(num==0){
									System.out.println("表:"+mtmTable+"的"+inverseJoinColumnName+" 列不存在!");
									continue;
								}
							}
							typeMap = jdbcTemplate.queryForMap("select column_type type from Information_schema.columns  where table_Name = '"+mtmTable+"' and column_name='"+joinColumnName+"' and table_schema = '"+table_schema+"'");
							if(typeMap.containsKey("TYPE")){
								comments.add("ALTER TABLE "+mtmTable+" MODIFY COLUMN "+joinColumnName+" "+typeMap.get("TYPE")+" COMMENT '"+ mtmcomment.joinColumn()+"' ");
								comments.add("ALTER TABLE "+mtmTable+" MODIFY COLUMN "+inverseJoinColumnName+" "+typeMap.get("TYPE")+" COMMENT '"+ mtmcomment.inverseJoinColumn()+"' ");
							}
						}
					}
				}
			}
		}
	}

	public List<String> getComments() {
		return comments;
	}
}
