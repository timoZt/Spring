package com.cloudJeesite.framework.frameworkCore.common.comment.annotation;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * 为每个实体表添加注释实现
 * @author psc
 *
 */
@Component
public class AddComment {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private CommentBase commentBase;
	@Autowired
	private JdbcTemplate jdbcTemplate;

	//添加注释开始
	public void addComment(){
		logger.info("开始添加注释");
		Date start = new Date();
		switch (commentBase.getDatabaseType()) {
		case "oracle":
			for(String sql:new OracleDataBase(jdbcTemplate,commentBase).getComments()){
				jdbcTemplate.execute(sql);
			}
			break;
		case "mysql":
			for(String sql:new MysqlDataBase(jdbcTemplate,commentBase).getComments()){
//				logger.info("执行注释sql:"+sql);
				jdbcTemplate.execute(sql);
			}
			break;
		case "sqlserver":
			for(String sql:initSqlServer()){
				jdbcTemplate.execute(sql);
			}
			break;
		default:
			break;
		}
		Date end = new Date();
		logger.info("注释添加完成,用时:"+((end.getTime()-start.getTime())/1000)+"秒");
	}
	/**
	 * Sql Server注释
	 * @return
	 */
	private List<String> initSqlServer(){
		List<String> sqlList = new ArrayList<String>();
		RedClasses dd = new RedClasses();
		String[] paths = commentBase.getPackagePath();
		for (String entityPath : paths) {
			Set<Class<?>> ss = dd.getClasses(entityPath);
			MyComment meta = null;
			String tableName = null;
			String columnName = null;
			for (Class<?> table : ss) {
				Table mm = table.getAnnotation(Table.class);
				if(mm!=null){
					tableName = mm.name();
					meta = table.getAnnotation(MyComment.class);
					if(meta!=null){
						sqlList.add("EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"+meta.value()+"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'"+tableName+"'");
					}
					for (Method me : table.getMethods()) {
						meta = me.getAnnotation(MyComment.class);
						Column column = me.getAnnotation(Column.class);
						ManyToOne mto = me.getAnnotation(ManyToOne.class);
						OneToOne oneToOne = me.getAnnotation(OneToOne.class);
						JoinColumn joinColumn = me.getAnnotation(JoinColumn.class);
						if(meta!=null&&(column!=null||joinColumn!=null&&(mto!=null||oneToOne!=null))){
							columnName = column == null ? joinColumn.name() : column.name();
							sqlList.add("EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"+columnName+"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'"+tableName+"'");
						}
						JoinTable joinTable = me.getAnnotation(JoinTable.class);
						ManyToManyComment mtmcomment = me.getAnnotation(ManyToManyComment.class);
						if(joinTable!=null&&mtmcomment!=null){
							String mtmTable = joinTable.name();
							String joinColumnName = joinTable.joinColumns()[0].name();
							String inverseJoinColumnName = joinTable.inverseJoinColumns()[0].name();
							sqlList.add("EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"+mtmcomment.tableComment()+"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'"+mtmTable+"'");
							sqlList.add("EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"+ mtmcomment.joinColumn()+"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'"+mtmTable+"', @level2type=N'COLUMN',@level2name=N'"+joinColumnName+"'");
							sqlList.add("EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"+ mtmcomment.inverseJoinColumn()+"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'"+mtmTable+"', @level2type=N'COLUMN',@level2name=N'"+inverseJoinColumnName+"'");
						}
					}
				}
			}
		}
		return sqlList;
	}
}

