package com.cloudJeesite.framework.frameworkCore.common.uitls.video;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.channels.FileChannel;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.MultimediaInfo;



/**
 * Created by timo on 2017/12/6.
 */
public class VideoUtils {

    @SuppressWarnings("unused")
	private String ReadVideoTime(File source) {
        Encoder encoder = new Encoder();
        String length = "";
        try {
            MultimediaInfo m = encoder.getInfo(source);
            long ls = m.getDuration()/1000;
            int hour = (int) (ls/3600);
            int minute = (int) (ls%3600)/60;
            int second = (int) (ls-hour*3600-minute*60);
            length = hour+"'"+minute+"''"+second+"'''";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return length;
    }

    /**
     * 获取视频大小
     * @param source
     * @return
     */
    @SuppressWarnings("unused")
    private String ReadVideoSize(File source) {
        FileChannel fc= null;
        String size = "";
        try {
            @SuppressWarnings("resource")
            FileInputStream fis = new FileInputStream(source);
            fc= fis.getChannel();
            BigDecimal fileSize = new BigDecimal(fc.size());
            size = fileSize.divide(new BigDecimal(1048576), 2, RoundingMode.HALF_UP) + "MB";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null!=fc){
                try{
                    fc.close();
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        }
        return size;
    }

    @SuppressWarnings({ "unchecked", "resource" })
	public static Map<String,Object> ReadVideoPar(File source){

        Encoder encoder = new Encoder();
        FileChannel fc= null;
        String size = "";
        Map<String,Object> map = new HashedMap();
        try {
            it.sauronsoftware.jave.MultimediaInfo m = encoder.getInfo(source);
            long ls = m.getDuration();
            map.put("videoTime",ls/60000+"分"+(ls)/1000+"秒");
            map.put("videoHigh",m.getVideo().getSize().getHeight());
            map.put("videoWidth",m.getVideo().getSize().getWidth());
            map.put("videoFormat",m.getFormat());
            FileInputStream fis = new FileInputStream(source);
            fc= fis.getChannel();
            BigDecimal fileSize = new BigDecimal(fc.size());
            size = fileSize.divide(new BigDecimal(1048576), 2, RoundingMode.HALF_UP) + "MB";
            map.put("videoSize",size);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (null!=fc){
                try {
                    fc.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return map;
    }

    /**
     * @Author: HONGLINCHEN
     * @Description:获取视频宽高大小时间
     * @Date: 2017-9-29 14:02
     */
    @SuppressWarnings("resource")
	public static void main(String[] args){
        //mac路径不支持~~
        File source = new File("/Users/timo/Downloads/333.mp4");
        Encoder encoder = new Encoder();
        FileChannel fc= null;
        String size = "";
        try {
            it.sauronsoftware.jave.MultimediaInfo m = encoder.getInfo(source);
            long ls = m.getDuration();
            System.out.println("此视频时长为:"+ls/60000+"分"+(ls)/1000+"秒！");
            System.out.println("此视频高度为:"+m.getVideo().getSize().getHeight());
            System.out.println("此视频宽度为:"+m.getVideo().getSize().getWidth());
            System.out.println("此视频格式为:"+m.getFormat());
            FileInputStream fis = new FileInputStream(source);
            fc= fis.getChannel();
            BigDecimal fileSize = new BigDecimal(fc.size());
            size = fileSize.divide(new BigDecimal(1048576), 2, RoundingMode.HALF_UP) + "MB";
            System.out.println("此视频大小为:"+size);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (null!=fc){
                try {
                    fc.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }





}
