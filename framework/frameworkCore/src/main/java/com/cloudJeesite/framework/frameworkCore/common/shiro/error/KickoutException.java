package com.cloudJeesite.framework.frameworkCore.common.shiro.error;

import org.apache.shiro.authc.AuthenticationException;

/**
 * 被踢出异常
 * Created by timo on 2017/11/23.
 */
public class KickoutException extends AuthenticationException {
    private static final long serialVersionUID = 1L;

    public KickoutException() {
        super();
    }

    public KickoutException(String message) {
        super(message);
    }
    public String getMSG(){
        return this.getMessage();
    }
}
