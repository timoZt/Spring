package com.cloudJeesite.framework.frameworkCore.common.uitls;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

/** 
* @author timo E-mail: 
* @version 创建时间：2018年4月28日 下午2:00:58 
* 类说明 
*/
@SuppressWarnings("unused")
public class PdfDemo {
	
	/*<dependency>
	    <groupId>com.itextpdf</groupId>
	    <artifactId>itextpdf</artifactId>
	    <version>5.5.10</version>
	</dependency>
	<dependency>
	    <groupId>com.itextpdf</groupId>
	    <artifactId>itext-asian</artifactId>
	    <version>5.2.0</version>
	</dependency>
	<!-- pdf密码 -->
	<dependency>
	    <groupId>org.bouncycastle</groupId>
	    <artifactId>bcprov-jdk15on</artifactId>
	    <version>1.54</version>
	</dependency>*/	
	
	  public static void addImg() throws DocumentException, MalformedURLException, IOException {
		  Document document = new Document();
          PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D:/test.pdf"));
          //图片1
	      Image image1 = Image.getInstance("D:/IMG_0555.JPG");
	      //设置图片位置的x轴和y周
	      image1.setAbsolutePosition(100f, 550f);
	      //设置图片的宽度和高度
	      image1.scaleAbsolute(200, 200);
	      //将图片1添加到pdf文件中
	      document.add(image1);
//	      //图片2
//	      Image image2 = Image.getInstance(new URL("http://static.cnblogs.com/images/adminlogo.gif"));
//	      //将图片2添加到pdf文件中
//	      document.add(image2);
          document.close();
	  } 
	  
	  public static void addTable() throws FileNotFoundException, DocumentException {
		  Document document = new Document();
          PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D:/test.pdf"));
          // 3列的表.
	      PdfPTable table = new PdfPTable(3); 
	      table.setWidthPercentage(100); // 宽度100%填充
	      table.setSpacingBefore(10f); // 前间距
	      table.setSpacingAfter(10f); // 后间距

	      java.util.List<PdfPRow> listRow = table.getRows();
	      //设置列宽
	      float[] columnWidths = { 1f, 2f, 3f };
	      table.setWidths(columnWidths);
	      //行1
	      PdfPCell cells1[]= new PdfPCell[3];
	      PdfPRow row1 = new PdfPRow(cells1);
	      //单元格
	      cells1[0] = new PdfPCell(new Paragraph("111"));//单元格内容
	      cells1[0].setBorderColor(BaseColor.BLUE);//边框验证
	      cells1[0].setPaddingLeft(20);//左填充20
	      cells1[0].setHorizontalAlignment(Element.ALIGN_CENTER);//水平居中
	      cells1[0].setVerticalAlignment(Element.ALIGN_MIDDLE);//垂直居中

	      cells1[1] = new PdfPCell(new Paragraph("222"));
	      cells1[2] = new PdfPCell(new Paragraph("333"));
	      //行2
	      PdfPCell cells2[]= new PdfPCell[3];
	      PdfPRow row2 = new PdfPRow(cells2);
	      cells2[0] = new PdfPCell(new Paragraph("444"));
	      //把第一行添加到集合
	      listRow.add(row1);
	      listRow.add(row2);
	      //把表格添加到文件中
	      document.add(table);
	      
          document.close();
	  } 
	  
	  
	  public static void addList() throws FileNotFoundException, DocumentException {
		  Document document = new Document();
          PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D:/test.pdf"));
          //添加有序列表
          // 4.添加一个内容段落
          List orderedList = new List(List.ORDERED);
	      orderedList.add(new ListItem("Item one"));
	      orderedList.add(new ListItem("Item two"));
	      orderedList.add(new ListItem("Item three"));
	      document.add(orderedList);
          document.close();
	  } 
	  
	  public static void addChinese() throws DocumentException, IOException {
		  Document document = new Document();
          PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D:/test.pdf"));
          //中文字体,解决中文不能显示问题
	      BaseFont bfChinese = BaseFont.createFont("STSong-Light","UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
	      //蓝色字体
	      Font blueFont = new Font(bfChinese);
	      blueFont.setColor(BaseColor.BLUE);
	      //段落文本
	      Paragraph paragraphBlue = new Paragraph("paragraphOne blue front", blueFont);
	      document.add(paragraphBlue);
	      //绿色字体
	      Font greenFont = new Font(bfChinese);
	      greenFont.setColor(BaseColor.GREEN);
	      //创建章节
	      Paragraph chapterTitle = new Paragraph("段落标题xxxx", greenFont);
	      Chapter chapter1 = new Chapter(chapterTitle, 1);
	      chapter1.setNumberDepth(0);
	      
	      Paragraph sectionTitle = new Paragraph("部分标题", greenFont);
	      Section section1 = chapter1.addSection(sectionTitle);
	      Paragraph sectionContent = new Paragraph("部分内容", blueFont);
	      section1.add(sectionContent);
	      //将章节添加到文章中
	      document.add(chapter1);
          document.close();
	  } 
	  
	  public static void addTxt() throws FileNotFoundException, DocumentException {
		  Document document = new Document();
          PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D:/test.pdf"));
          // 4.添加一个内容段落
	      document.add(new Paragraph("Hello World!"));
          document.close();
	  }    
	  
	  public static void setAttributes() throws FileNotFoundException, DocumentException {
		  Document document = new Document();
          PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D:/test.pdf"));
          //设置属性
	      //标题
	      document.addTitle("this is a title");
	      //作者
	      document.addAuthor("H__D");
	      //主题
	      document.addSubject("this is subject");
	      //关键字
	      document.addKeywords("Keywords");
	      //创建时间
	      document.addCreationDate();
	      //应用程序
	      document.addCreator("hd.com");
          document.close();
	  }
	  
	  public static void addPassWord() throws FileNotFoundException, DocumentException {
		  	// 1.新建document对象
	        Document document = new Document();
	        // 2.建立一个书写器(Writer)与document对象关联，通过书写器(Writer)可以将文档写入到磁盘中。
	        // 创建 PdfWriter 对象 第一个参数是对文档对象的引用，第二个参数是文件的实际名称，在该名称中还会给出其输出路径。
	        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D:/test.pdf"));
	        // 3.打开文档
	        document.open();
	        //用户密码
	        String userPassword = "123456";
	        //拥有者密码
	        String ownerPassword = "hd";
	        writer.setEncryption(userPassword.getBytes(), ownerPassword.getBytes(), PdfWriter.ALLOW_PRINTING,
	                PdfWriter.ENCRYPTION_AES_128);
	        // 打开文件
	        document.open();
	        //添加内容
	        document.add(new Paragraph("password !!!!"));
	        
	        // 只读权限
	        writer.setEncryption("".getBytes(), "".getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
	        // 打开文件
	        document.open();
	        // 添加内容
	        document.add(new Paragraph("password !!!!"));
	        // 5.关闭文档
	        document.close();
	  }
	  
	  public static void updatePdf() throws MalformedURLException, IOException, DocumentException {
	        PdfReader pdfReader = new PdfReader("C:/Users/H__D/Desktop/test1.pdf");//读取pdf文件
	        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileOutputStream("C:/Users/H__D/Desktop/test10.pdf"));  //修改器
	       
	        Image image = Image.getInstance("C:/Users/H__D/Desktop/IMG_0109.JPG");
	        image.scaleAbsolute(50, 50);
	        image.setAbsolutePosition(0, 700);
	     
	        for(int i=1; i<= pdfReader.getNumberOfPages(); i++){
	            PdfContentByte content = pdfStamper.getUnderContent(i);
	            content.addImage(image);
	        }
	        pdfStamper.close();
	  }
}
