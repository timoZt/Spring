/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.cloudJeesite.framework.frameworkCore.common.uitls;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * 对象操作工具类, 继承org.apache.commons.lang3.ObjectUtils类
 * @author ThinkGem
 * @version 2014-6-29
 */
public class ObjectUtils extends org.apache.commons.lang3.ObjectUtils {

	/**
	 * 注解到对象复制，只复制能匹配上的方法。
	 * @param annotation
	 * @param object
	 */
	public static void annotationToObject(Object annotation, Object object){
		if (annotation != null){
			Class<?> annotationClass = annotation.getClass();
			if (null == object) {
				return;
			}
			Class<?> objectClass = object.getClass();
			for (Method m : objectClass.getMethods()){
				if (StringUtils.startsWith(m.getName(), "set")){
					try {
						String s = StringUtils.uncapitalize(StringUtils.substring(m.getName(), 3));
						Object obj = annotationClass.getMethod(s).invoke(annotation);
						if (obj != null && !"".equals(obj.toString())){
							m.invoke(object, obj);
						}
					} catch (Exception e) {
						// 忽略所有设置失败方法
					}
				}
			}
		}
	}



	/**
	 * 序列化对象
	 * @param object
	 * @return
	 */
	public static byte[] serialize(Object object) {
		ObjectOutputStream oos = null;
		ByteArrayOutputStream baos = null;
		try {
			if (object != null){
				baos = new ByteArrayOutputStream();
				oos = new ObjectOutputStream(baos);
				oos.writeObject(object);
				return baos.toByteArray();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 反序列化对象
	 * @param bytes
	 * @return
	 */
	public static Object unserialize(byte[] bytes) {
		ByteArrayInputStream bais = null;
		try {
			if (bytes != null && bytes.length > 0){
				bais = new ByteArrayInputStream(bytes);
				ObjectInputStream ois = new ObjectInputStream(bais);
				return ois.readObject();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	
	/**
	 * 得到对象的字符值 
	 * @param o
	 * @return
	 */
	public static String getObjStr(Object o) {
		return (o != null ? o.toString() : "");
	}

	/**
	 * 得到对象的整形值，转化失败返回 0
	 * @param o
	 * @return
	 */
	public static Integer getObjInteger(Object o) {
		Integer oi = new Integer(0);
		if (o instanceof Integer) {
			oi = (Integer) o;
		}
		return oi;
	}

	/**
	 * 得到对象的长整形值，转化失败返回 0
	 * @param o
	 * @return
	 */
	public static Long getObjLong(Object o) {
		Long ol = new Long(0);
		if (o instanceof Long) {
			ol = (Long) o;
		}
		return ol;
	}

	/**
	 *  得到对象的浮点形式值，转化失败返回 0.0
	 * @param o
	 * @return
	 */
	public static Double getObjDouble(Object o) {
		Double od = new Double(0.0);
		if (o instanceof Double) {
			od = (Double) o;
		}else if(o instanceof BigDecimal){
			od=((BigDecimal) o).doubleValue();
		}
		return od;
	}

	/**
	 * 得到对象的日期值，转化失败返回null
	 * @param o
	 * @return
	 */
	public static Date getObjDate(Object o) {
		Date date = null;
		if (o instanceof Date) {
			date = (Date) o;
		}
		return date;
	}

}
