# cloudJeesite

#### 项目介绍
在bootJeesite的基础上做分布式微服务转换
bootJeesite:https://gitee.com/timoZt/BootJeesite
#### 软件架构
![输入图片说明](https://gitee.com/uploads/images/2018/0504/155738_4c99230a_731572.png "20180104142729219.png")

#### 安装教程
项目使用了rabbitMq做消息中间件
用Spring Cloud Bus实现通知微服务架构的配置文件的更改--可以不使用
文件在附件中下载

针对windos：
1. 先安装otp_win64_20.3 再安装rabbitMq
2. set ERLANG_HOME=C:\Program Files\erl9.3 命令行设置环境变量
3. 到rabbitMq目录下输入： cd sbin
4. .\rabbitmq-plugins.bat enable rabbitmq_management 	要加.\

针对其他环境请自行百度

#### 项目说明

1.component springcloud基础服务
- eurekaServer 服务注册中心
- configServe 分布式服务配置中心 这里使用了git的远程仓库
    在需要使用的地方进行配置 @RefreshScope//动态mq通知刷新必备 注解到获取配制文件的类上
```
management:
  security:
    enabled: false
spring:
  application:
    name: eureka-consumer 
  cloud:
    config:
      label: master
      profile: dev
      discovery:     
        enabled: true
        serviceId: config-server
```
- gatewayService :Spring Cloud Sleuth 个人理解为配置了服务链路追踪的网关
- turbineServer 断路器聚合监控(Hystrix Turbine) 配合断路器使用
    在服务中心开启后可单独使用： ‘http://localhost:port（需要监控的端口的ip）/hystrix,输入监控流http://localhost:8769/turbine.stream’ 需要被监控的服务加入对应包和注解，对应服务必须有断路器 如consumer中加入的：@EnableHystrix @EnableHystrixDashboard
    ```
    <!-- 断路器 -->
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-hystrix</artifactId>
    </dependency>
    <!-- 断路监控 https://blog.csdn.net/forezp/article/details/69934399-->
    <!-- 未集成 断路器聚合监控(Hystrix Turbine)https://blog.csdn.net/forezp/article/details/70233227 -->
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-hystrix-dashboard</artifactId>
    </dependency>
    
    ```


- zipkinServer 服务链路追踪(Spring Cloud Sleuth) 
    
    在服务中心开启后可单独使用： ‘http://localhost:port’直接访问。对需要进行链路追踪的服务进行配置
```
     spring.zipkin.base-url=http://localhost:9411

    xml：
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-zipkin</artifactId>
    </dependency>
```

2.service 业务库
- demo 示例
1. clientOne 客户端 服务提供者
2. clientTwo 客户端 服务提供者 和客户端1注册为同一服务名进行负载均衡
3. consumer 服务消费者 两种不同调用方式演示



#### 使用说明

1.启动顺序
    保证eurekaServer先行启动即可
